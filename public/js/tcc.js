document.addEventListener("DOMContentLoaded", function(event) {
    // CONTROLE DO MENU
    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
        const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId)

        if(toggle && nav && bodypd && headerpd){
            toggle.addEventListener('click', ()=>{
                nav.classList.toggle('show')
                toggle.classList.toggle('bx-x')
                bodypd.classList.toggle('body-pd')
                headerpd.classList.toggle('body-pd')
            })
        }
    }

    showNavbar('header-toggle','nav-bar','body-pd','header')

    // MASCARAS
    var mascaraTelefone = {
        'placeholder': '(00) 0 0000-0000',
        onKeyPress: function(tel, e, field, options) {
          // Use an optional digit (9) at the end to trigger the change
          var masks = ["(00) 0000-00009", "(00) 0 0000-0000"],
            digits = tel.replace(/[^0-9]/g, "").length,

            mask = digits <= 10 ? masks[0] : masks[1];

          $(".phone").mask(mask, options);
        }
      };

    $('.cpf').mask('000.000.000-00', {
        reverse: true,
        placeholder: '000.000.000-00'
    });
    $('.cnpj').mask('00.000.000/0000-00', {
        reverse: true,
        placeholder: '00.000.000/0000-00'
    });
    $('.money').mask('0.000.000,00', {
        reverse: true,
        placeholder: '1.000,00'
    });
    $('.time').mask('AB:CD', {
        'translation': {
            A: {pattern: /[0-2]/},
            B: {pattern: /[0-9]/},
            C: {pattern: /[0-5]/},
            D: {pattern: /[0-9]/},
        },
        'placeholder': '00:00'
    });
    $('.date_time').mask('00/00/0000 AB:CD', {
        'translation': {
            A: {pattern: /[0-2]/},
            B: {pattern: /[0-9]/},
            C: {pattern: /[0-5]/},
            D: {pattern: /[0-9]/},
        },
        'placeholder': 'dd/mm/aaaa 00:00'
    });
    $('.phone').mask('(00) 0 0000-0000', mascaraTelefone);

    // TRATAMENTO DO FORMULARIO
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()

    $(document).on('click', '#desativar', function() {
        $('#modal-buttons').html(`
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            <a href="${$(this).data('url')}" class="btn btn-primary">Confirmar</a>
        `);
    });
});
