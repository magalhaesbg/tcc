<?php

namespace Database\Seeders;

use App\Models\FluxoCaixa;
use Illuminate\Database\Seeder;

class FluxoCaixaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['data' => '2022-04-01', 'descricao' => 'Venda de shamppo',                 'valor' => '10.00', 'tipo_fluxo_id' => 1],
            ['data' => '2022-04-02', 'descricao' => 'Venda de máscara capilar',         'valor' => '20.50', 'tipo_fluxo_id' => 1],
            ['data' => '2022-04-02', 'descricao' => 'Venda de condionador para barba',  'valor' => '15.90', 'tipo_fluxo_id' => 1],

            ['data' => '2022-04-10', 'descricao' => 'Pagamento conta de luz',   'valor' => '200.00', 'tipo_fluxo_id' => 2],
            ['data' => '2022-04-10', 'descricao' => 'Pagamento conta de água',  'valor' => '110.00', 'tipo_fluxo_id' => 2],
            ['data' => '2022-04-15', 'descricao' => 'Compra de suprimentos',    'valor' => '300.00', 'tipo_fluxo_id' => 2],

        ];

        FluxoCaixa::insert($arDados);
    }
}
