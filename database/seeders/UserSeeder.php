<?php

namespace Database\Seeders;

use App\Models\TipoFeriado;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['name' => 'Felipe Magalhães',  'cpf' => '00000000000', 'email' => 'magalhaesbg@felipemagalhaes.com',   'changed_password' => NULL, 'password' => Hash::make('12345678')],

            ['name' => 'Fernando',          'cpf' => '11111111111', 'email' => 'fernando@felipemagalhaes.com',      'changed_password' => date('Y-m-d'), 'password' => Hash::make('12345678')],
            ['name' => 'André',             'cpf' => '22222222222', 'email' => 'andre@felipemagalhaes.com',         'changed_password' => date('Y-m-d'), 'password' => Hash::make('12345678')],

            ['name' => 'Cláudio',           'cpf' => '33333333333', 'email' => 'claudio@felipemagalhaes.com',       'changed_password' => date('Y-m-d'), 'password' => Hash::make('12345678')],
            ['name' => 'Paulo',             'cpf' => '44444444444', 'email' => 'paulo@felipemagalhaes.com',         'changed_password' => date('Y-m-d'), 'password' => Hash::make('12345678')],

            ['name' => 'Ricardo',           'cpf' => '55555555555', 'email' => 'ricardo@felipemagalhaes.com',       'changed_password' => NULL, 'password' => Hash::make('12345678')],
            ['name' => 'João',              'cpf' => '66666666666', 'email' => 'joao@felipemagalhaes.com',          'changed_password' => NULL, 'password' => Hash::make('12345678')],
        ];

        User::insert($arDados);
    }
}
