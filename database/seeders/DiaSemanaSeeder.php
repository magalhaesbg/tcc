<?php

namespace Database\Seeders;

use App\Models\DiaSemana;
use Illuminate\Database\Seeder;

class DiaSemanaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['nome' => 'Domingo',        'ordem_semana' => 0],
            ['nome' => 'Segunda-Feira',  'ordem_semana' => 1],
            ['nome' => 'Terça-Feira',    'ordem_semana' => 2],
            ['nome' => 'Quarta-Feira',   'ordem_semana' => 3],
            ['nome' => 'Quinta-Feira',   'ordem_semana' => 4],
            ['nome' => 'Sexta-Feira',    'ordem_semana' => 5],
            ['nome' => 'Sábado',         'ordem_semana' => 6],
        ];

        DiaSemana::insert($arDados);
    }
}
