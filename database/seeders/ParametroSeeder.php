<?php

namespace Database\Seeders;

use App\Models\Parametro;
use Illuminate\Database\Seeder;

class ParametroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['nome' => 'Barbearia Barba Negra',  'cnpj' => '79783725000153', 'numero_fidelidade' => 10],
        ];

        Parametro::insert($arDados);
    }
}
