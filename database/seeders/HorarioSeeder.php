<?php

namespace Database\Seeders;

use App\Models\Horario;
use Illuminate\Database\Seeder;

class HorarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            // ['parametro_id' => 1, 'dia_semana_id' => 1, 'entrada_manha' => '', 'saida_manha' => '', 'entrada_tarde' => '', 'saida_tarde' => ''],
            ['parametro_id' => 1, 'dia_semana_id' => 2, 'entrada_manha' => '08:00', 'saida_manha' => '12:00', 'entrada_tarde' => '14:00', 'saida_tarde' => '17:00'],
            ['parametro_id' => 1, 'dia_semana_id' => 3, 'entrada_manha' => '08:00', 'saida_manha' => '12:00', 'entrada_tarde' => '14:00', 'saida_tarde' => '17:00'],
            ['parametro_id' => 1, 'dia_semana_id' => 4, 'entrada_manha' => '08:00', 'saida_manha' => '12:00', 'entrada_tarde' => '14:00', 'saida_tarde' => '17:00'],
            ['parametro_id' => 1, 'dia_semana_id' => 5, 'entrada_manha' => '08:00', 'saida_manha' => '12:00', 'entrada_tarde' => '14:00', 'saida_tarde' => '17:00'],
            ['parametro_id' => 1, 'dia_semana_id' => 6, 'entrada_manha' => '08:00', 'saida_manha' => '12:00', 'entrada_tarde' => '14:00', 'saida_tarde' => '17:00'],
            ['parametro_id' => 1, 'dia_semana_id' => 7, 'entrada_manha' => '09:00', 'saida_manha' => '13:00', 'entrada_tarde' => NULL, 'saida_tarde' => NULL],
        ];

        Horario::insert($arDados);
    }
}
