<?php

namespace Database\Seeders;

use App\Models\TipoFluxo;
use Illuminate\Database\Seeder;

class TipoFluxoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['nome' => 'Entrada'],
            ['nome' => 'Saída'],
        ];

        TipoFluxo::insert($arDados);
    }
}
