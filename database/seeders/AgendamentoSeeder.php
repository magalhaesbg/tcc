<?php

namespace Database\Seeders;

use App\Models\Agendamento;
use App\Services\AgendamentoService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class AgendamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();

        $agendamentoService = App::make(AgendamentoService::class);

        $arDados = [
            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-02", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-02", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-04", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-04", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-06", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-06", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-10", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-10", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-12", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-12", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-16", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-16", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-18", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-18", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-20", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-20", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-24", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-24", "hora" => "14:40"],

            ["user_id" => "6", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-26", "hora" => "09:40"],
            ["user_id" => "7", "barbeiro" => "3", "servicos" => ['4' => true, '5' => true], "tempo_estimado" => "25", "dia" => "2022-05-26", "hora" => "14:40"],

        ];

        foreach($arDados AS $dados) {
            request()->merge($dados);

            $agendamentoService->store(request());
        }

        for($i = 1; $i < 21; $i++) {
            $agendamentoService->concluido($i);
        }
    }
}






