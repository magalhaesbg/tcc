<?php

namespace Database\Seeders;

use App\Models\TipoFeriado;
use Illuminate\Database\Seeder;

class TipoFeriadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['nome' => 'Feriado'],
            ['nome' => 'Folga Coletiva'],
            ['nome' => 'Outros'],

        ];

        TipoFeriado::insert($arDados);
    }
}
