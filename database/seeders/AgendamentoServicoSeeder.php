<?php

namespace Database\Seeders;

use App\Models\AgendamentoServico;
use Illuminate\Database\Seeder;

class AgendamentoServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['valor' => '20.00', 'servico_id' => 1, 'agendamento_id' => 1,],
        ];

        AgendamentoServico::insert($arDados);
    }
}
