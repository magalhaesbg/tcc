<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            UserRoleSeeder::class,

            TipoFeriadoSeeder::class,
            FeriadoSeeder::class,
            ServicoSeeder::class,
            TipoFluxoSeeder::class,
            UserServicoSeeder::class,
            DiaSemanaSeeder::class,
            ParametroSeeder::class,
            HorarioSeeder::class,
            TipoAusenteSeeder::class,
            FluxoCaixaSeeder::class,
            AgendamentoSeeder::class,
            AgendamentoServicoSeeder::class
        ]);
    }
}
