<?php

namespace Database\Seeders;

use App\Models\TipoAusente;
use Illuminate\Database\Seeder;

class TipoAusenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['nome' => 'Ferias'],
            ['nome' => 'Folga'],
            ['nome' => 'Não Justificado'],
            ['nome' => 'Médico'],
            ['nome' => 'Outros'],
        ];

        TipoAusente::insert($arDados);
    }
}
