<?php

namespace Database\Seeders;

use App\Models\UserServico;
use Illuminate\Database\Seeder;

class UserServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['user_id' => '2',  'servico_id' => '1'],
            ['user_id' => '2',  'servico_id' => '2'],
            ['user_id' => '2',  'servico_id' => '3'],

            ['user_id' => '3',  'servico_id' => '4'],
            ['user_id' => '3',  'servico_id' => '5'],
            ['user_id' => '3',  'servico_id' => '1'],
        ];

        UserServico::insert($arDados);
    }
}
