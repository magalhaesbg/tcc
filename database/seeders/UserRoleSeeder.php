<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::find(1)->assignRole('Gerente');

        User::find(2)->assignRole('Barbeiro');
        User::find(3)->assignRole('Barbeiro');

        User::find(4)->assignRole('Atendente');
        User::find(5)->assignRole('Atendente');

        User::find(6)->assignRole('Cliente');
        User::find(7)->assignRole('Cliente');

    }
}
