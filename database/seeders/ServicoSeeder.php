<?php

namespace Database\Seeders;

use App\Models\Servico;
use Illuminate\Database\Seeder;

class ServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arDados = [
            ['nome' => 'Corte com máquina', 'valor' => '20.00', 'tempo_estimado' => '10'],
            ['nome' => 'Corte com tesoura', 'valor' => '30.00', 'tempo_estimado' => '20'],
            ['nome' => 'Barba',             'valor' => '30.00', 'tempo_estimado' => '20'],
            ['nome' => 'Corte artístico',   'valor' => '25.00', 'tempo_estimado' => '15'],
            ['nome' => 'Sombrancelha',      'valor' => '10.00', 'tempo_estimado' => '10'],
        ];

        Servico::insert($arDados);
    }
}
