<?php

use App\Models\DiaSemana;
use App\Models\Parametro;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->id();
            $table->time('entrada_manha')->nullable();
            $table->time('saida_manha')->nullable();
            $table->time('entrada_tarde')->nullable();
            $table->time('saida_tarde')->nullable();
            $table->foreignId('dia_semana_id')->references('id')->on('dia_semanas')->onDelete('cascade');
            $table->foreignId('parametro_id')->references('id')->on('parametros')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
};
