<?php

use App\Models\Agendamento;
use App\Models\TipoFluxo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fluxo_caixas', function (Blueprint $table) {
            $table->id();
            $table->dateTime('data');
            $table->string('descricao')->nullable();
            $table->decimal('valor', 6, 2);
            $table->foreignId('agendamento_id')->nullable()->references('id')->on('agendamentos')->onDelete('cascade');
            $table->foreignId('tipo_fluxo_id')->references('id')->on('tipo_fluxos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fluxo_caixas');
    }
};
