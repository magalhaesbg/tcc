<?php

use App\Models\Agendamento;
use App\Models\Servico;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendamento_servicos', function (Blueprint $table) {
            $table->id();
            $table->double('valor', 6, 2);
            $table->foreignId('servico_id')->references('id')->on('servicos')->onDelete('cascade');
            $table->foreignId('agendamento_id')->references('id')->on('agendamentos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendamento_servicos');
    }
};
