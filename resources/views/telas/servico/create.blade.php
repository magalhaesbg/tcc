@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Novo Serviço',
        'subtitulo' => '',
        'route'     => 'servico.store',
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'nome',           'id' => 'nome',           'required' => true, 'maxLength' => '45'],
            ['component' => MONEY,      'label' => 'Valor',          'name' => 'valor',          'id' => 'valor',          'required' => true],
            ['component' => INTEGER,    'label' => 'Tempo Estimado', 'name' => 'tempo_estimado', 'id' => 'tempo_estimado', 'required' => true],
        ]
    ])

@endsection
