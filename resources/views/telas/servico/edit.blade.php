@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'titulo'    => 'Editar Serviço',
        'route'     => 'servico.update',
        'id'        => $servico->id,
        'isEdit'    => true,
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'nome',           'id' => 'nome',           'value' => $servico->nome,           'required' => true, 'maxLength' => '45'],
            ['component' => MONEY,      'label' => 'Valor',          'name' => 'valor',          'id' => 'valor',          'value' => $servico->valor,          'required' => true],
            ['component' => INTEGER,    'label' => 'Tempo Estimado', 'name' => 'tempo_estimado', 'id' => 'tempo_estimado', 'value' => $servico->tempo_estimado, 'required' => true],
        ]
    ])
{{-- {{dd($servico)}} --}}
@endsection
