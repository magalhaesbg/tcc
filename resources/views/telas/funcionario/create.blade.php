@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Novo Funcionário',
        'subtitulo' => '',
        'route'     => 'funcionario.store',
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'name',           'id' => 'name',                      'required' => true, 'maxLength' => '140'],
            ['component' => CPF,        'label' => 'CPF',            'name' => 'cpf',            'id' => 'cpf',                       'required' => true, 'maxLength' => '14'],
            ['component' => INPUT_TEXT, 'label' => 'Email',          'name' => 'email',          'id' => 'email',                     'required' => true, 'maxLength' => '140'],
            ['component' => SELECT,     'label' => 'Função',         'name' => 'role',           'id' => 'role',                      'required' => true, 'valores' => $funcoes],
            ['component' => CHECKBOX,   'label' => 'Serviços',       'name' => 'servicos',       'id' => 'servicos',                  'required' => true, 'valores' => $servicos],
        ]
    ])

    {{-- JAVACRIPT ADICIONAL BARBEIRO HABILITA SERVICOS - ATENDENTE DESABILITA SERVICOS  --}}
    <script>
        $(document).ready(function() {
            $('#role').on('change', function() {
                if ( $(this).val() === "2" ) {
                    $("input[name^='servicos']").each(function(i){
                        $(this).removeAttr('disabled');
                    })
                } else {
                    $("input[name^='servicos']").each(function(i){
                        $(this).attr('disabled', 'disabled');
                        $(this).prop('checked', false);
                    })
                }
            });

            $('#role').change();
        });
    </script>
@endsection
