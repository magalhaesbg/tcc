<header class="header" id="header">
    <div class="header_toggle">
        <i class='bi bi-list' id="header-toggle"></i>
    </div>
    <div>
        Olá, {{ Auth::user()->name }}
    </div>
    <div class="header_img">
        {{-- <img src="https://i.imgur.com/hczKIze.jpg" alt=""> --}}
    </div>
</header>
<div class="l-navbar" id="nav-bar">
    <nav class="nav">
        <div>
            <a href="#" class="nav_logo">
                <i class='bi bi-layers nav_logo-icon'></i>
                <span class="nav_logo-name">Logo Aqui</span>
            </a>
            <div class="nav_list">
                <a href="{{ route('dashboard') }}" class="nav_link {{ (request()->is('dashboard')) ? 'active' : '' }}">
                    <i class='bi bi-columns-gap nav_icon'></i>
                    <span class="nav_name">Dashboard</span>
                </a>
                <a href="{{ route('agendamento.index') }}" class="nav_link {{ (request()->is('agendamento')) ? 'active' : '' }}">
                    <i class="bi bi-calendar3 nav_icon"></i>
                    <span class="nav_name">Agendamentos</span>
                </a>
                @role('Gerente|Barbeiro|Atendente')
                    <a href="{{ route('cliente.index') }}" class="nav_link {{ (request()->is('cliente')) ? 'active' : '' }}">
                        <i class='bi bi-person-circle nav_icon'></i>
                        <span class="nav_name">Clientes</span>
                    </a>
                @endrole
                @role('Gerente')
                    <a href="{{ route('fluxocaixa.index') }}" class="nav_link {{ (request()->is('fluxo_caixa')) ? 'active' : '' }}">
                        <i class='bi bi-cash-stack nav_icon'></i>
                        <span class="nav_name">Fluxo de Caixa</span>
                    </a>
                    <a href="{{ route('funcionario.index') }}" class="nav_link {{ (request()->is('funcionario')) ? 'active' : '' }}">
                        <i class='bi bi-people-fill nav_icon'></i>
                        <span class="nav_name">Funcionários</span>
                    </a>
                    <a href="{{ route('servico.index') }}" class="nav_link {{ (request()->is('servico')) ? 'active' : '' }}">
                        <i class='bi bi-card-list nav_icon'></i>
                        <span class="nav_name">Serviços</span>
                    </a>
                    <hr style="height:2px;color:white;">
                    <a class="nav_link">
                        <i class='bi bi-gear nav_icon'></i>
                        <span class="nav_name">Configurações</span>
                    </a>
                    <a href="{{ route('configuracao.parametro') }}" class="nav_link {{ (request()->is('configuracao/parametro')) ? 'active' : '' }}">
                        <i class='bi bi-list-check nav_icon'></i>
                        <span class="nav_name">Parâmetros</span>
                    </a>
                    <a href="{{ route('configuracao.feriado') }}" class="nav_link {{ (request()->is('configuracao/feriado')) ? 'active' : '' }}">
                        <i class='bi bi-calendar-event-fill nav_icon'></i>
                        <span class="nav_name">Feriados</span>
                    </a>
                @endrole
            </div>
        </div>
        <a href="javascript:void" onclick="$('#logout-form').submit();" class="nav_link">
            <i class='bi bi-box-arrow-right nav_icon'></i>
            <span class="nav_name">Sair</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </nav>
</div>
<div class="height-100 bg-light">
    <div class="content">
        <div class="container-fluid">
            <div class="row pt-4">
                @yield('content')
            </div>
        </div>
    </div>
</div>
