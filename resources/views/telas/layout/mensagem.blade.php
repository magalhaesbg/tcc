@if (Session::has('mensagem'))
    @if (Session::get('tipo') === 'success')
        <div class="alert alert-success" role="alert">
            {{ Session::get('mensagem') }}
        </div>
    @else
        <div class="alert alert-danger" role="alert">
            {{ Session::get('mensagem') }}
        </div>
    @endif
@endif
