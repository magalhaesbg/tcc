@extends('layouts.app')

@section('head-js')
    <script src="{{ asset('js/jquery-dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables-responsive.min.js') }}"></script>
@endsection

@section('head-css')
    <link rel="stylesheet" href="{{ asset('css/jquery-dataTables.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/responsive-dataTable.min.css') }}" type="text/css">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <div class='row'>
                <div class='col-6 row align-items-center'>
                    <h4 class='mb-0'>Agendamentos</h4>
                </div>
                <div class='col-6 d-flex flex-row-reverse'>
                    <a href="{{ route('agendamento.add') }}" class="btn btn-success">
                        <i class="bi bi-plus-lg"></i>
                        Agendamento
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('telas.layout.mensagem')
            <table class="table table-hover data-table" style="width: 100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Cliente</th>
                        <th>Barbeiro</th>
                        <th>Data de Inicio</th>
                        <th>Data Prevista Término</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th width="200px"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            var table = $('.data-table').DataTable({
                oLanguage: {
                    sUrl: 'datatable/language.txt'
                },
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('agendamento.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'user.name', name: 'user.name'},
                    {data: 'barbeiro.name', name: 'barbeiro.name'},
                    {data: 'dia_hora_inicio', name: 'dia_hora_inicio'},
                    {data: 'dia_hora_fim', name: 'dia_hora_fim'},
                    {data: 'valor', name: 'valor'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {
                        targets: 4,
                        className: 'text-center'
                    },
                    {
                        targets: 5,
                        className: 'text-center'
                    }
                ],
                order: [
                    [1, 'asc']
                ]
            });

            window.onresize = function() {
                table.columns.adjust().draw();
            }
        });

    </script>
@endsection
