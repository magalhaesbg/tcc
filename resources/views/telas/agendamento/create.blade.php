@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @php
        $campos = [];

        if (Auth::user()->hasRole(['Gerente|Atendente|Barbeiro'])) {
            $campos[] = ['component' => SELECT, 'label' => 'Cliente', 'name' => 'user_id', 'id' => 'user_id', 'required' => true, 'valores' => $clientes, 'descricao_option' => 'nameEmail'];
        }

        $campos[] = ['component' => SELECT,     'label' => 'Barbeiro',              'name' => 'barbeiro',       'id' => 'barbeiro',                  'required' => true, 'valores' => $barbeiros];
        $campos[] = ['component' => CHECKBOX,   'label' => 'Serviços',              'name' => 'servicos',       'id' => 'servicos',                  'required' => true, 'valores' => $servicos, 'disabled' => true, 'data' => ['valor', 'tempo_estimado']];
        $campos[] = ['component' => MONEY,      'label' => 'Valor',                 'name' => 'valor',          'id' => 'valor',                     'required' => true, 'disabled' => true, 'value' => '0,00'];
        $campos[] = ['component' => INTEGER,    'label' => 'Tempo estimado',        'name' => 'tempo_estimado', 'id' => 'tempo_estimado',            'required' => true, 'readonly' => true, 'value' => '0'];
        $campos[] = ['component' => DATE,       'label' => 'Dia',                   'name' => 'dia',            'id' => 'dia',                       'required' => true, 'min' => date('Y-m-d')];
        $campos[] = ['component' => SELECT,     'label' => 'Horário',               'name' => 'hora',           'id' => 'hora',                      'required' => true, 'valores' => []];

    @endphp

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Novo Agendamento',
        'subtitulo' => '',
        'route'     => 'agendamento.store',
        'campos'        => $campos
    ])

    <script>
        $('#barbeiro').on('change', function() {
            $.ajax({
                url : "{{ route('funcionario.servico') }}",
                type : 'post',
                data : {
                    _token: "{{csrf_token()}}",
                    barbeiro_id : function() { return $('#barbeiro option:selected').val() },
                }
            })
            .done(function(msg){
                msg = JSON.parse(msg);

                $("input[name^='servicos']").attr('disabled', 'disabled');

                $.each(msg, function(key, value){
                    $('#servicos_'+value['servico_id']).removeAttr('disabled');
                });
            })
        });

        $('input[name^="servico"]').on('change', function() {
            $('#dia').val('').trigger('input');
            $('#hora').html('<option value="">Selecione...</option>');

            var valor = $('#valor').val();
            valor = valor.replace('.', '');
            valor = valor.replace(',', '.');

            var valorServico = $(this).data('valor');
            valorServico = valorServico.replace('.', '');
            valorServico = valorServico.replace(',', '.');

            var tempoEstimado = $('#tempo_estimado').val();

            var tempoEstimadoServico = $(this).data('tempo_estimado');

            if ( $(this).is(':checked') ) {
                var total = parseFloat(valorServico) + parseFloat(valor);
                var tempoEstimadoTotal = parseInt(tempoEstimado) + parseInt(tempoEstimadoServico);
            } else {
                var total = parseFloat(valorServico) - parseFloat(valor);
                var tempoEstimadoTotal = parseInt(tempoEstimado) - parseInt(tempoEstimadoServico);
            }

            $('#valor').val(total.toFixed(2)).trigger('input');
            $('#tempo_estimado').val(tempoEstimadoTotal).trigger('input');
        });

        $('#dia').on('change', function() {
            $('#msg_dia_remove').remove();

            $.ajax({
                url : "{{ route('agendamento.horario') }}",
                type : 'post',
                data : {
                    _token: "{{csrf_token()}}",
                    dia: function() { return $('#dia').val() },
                    barbeiro: function() { return $('#barbeiro option:selected').val() },
                    tempo: function() { return $('#tempo_estimado').val() },
                }
            })
            .done(function(msg){
                msg = JSON.parse(msg);

                if (msg.status === 'erro') {
                    $('#dia').parent().append("<div id='msg_dia_remove' class='invalid-feedback' style='display:block'>"+msg.message+"</div>")
                } else {
                    $('#hora').html("");
                    $('#hora').append('<option value="">Selecione...</option>');
                    msg.forEach(function(e, i) {
                        $('#hora').append('<option value="'+e+'">'+e+'</option>');
                    });
                }
            })
        });
    </script>
@endsection
