@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @php
        $campos = [];

        if (Auth::user()->hasRole(['Gerente|Atendente|Barbeiro'])) {
            $campos[] = ['component' => INPUT_TEXT, 'label' => 'Cliente', 'name' => 'user_id', 'id' => 'user_id', 'maxLength' => '200', 'disabled' => true, 'required' => true, 'value' => $agendamento->user->name .' - ('. $agendamento->user->cpf .')', 'descricao_option' => 'nameEmail'];
        }

        $campos[] = ['component' => INPUT_TEXT, 'label' => 'Barbeiro',              'name' => 'barbeiro',       'id' => 'barbeiro',            'maxLength' => '200',    'disabled' => true,      'required' => true, 'value' => $agendamento->barbeiro->name];
        $campos[] = ['component' => CHECKBOX,   'label' => 'Serviços',              'name' => 'servicos',       'id' => 'servicos',            'disabled' => true,      'required' => true, 'valores' => $servicos, 'value' => $agendamento->agendamentoServico->pluck('servico_id'), 'data' => ['valor', 'tempo_estimado']];
        $campos[] = ['component' => MONEY,      'label' => 'Valor',                 'name' => 'valor',          'id' => 'valor',               'disabled' => true,      'required' => true, 'value' => '0,00'];
        $campos[] = ['component' => INTEGER,    'label' => 'Tempo estimado',        'name' => 'tempo_estimado', 'id' => 'tempo_estimado',      'disabled' => true,      'required' => true, 'readonly' => true, 'value' => '0'];
        $campos[] = ['component' => DATE,       'label' => 'Dia',                   'name' => 'dia',            'id' => 'dia',                 'disabled' => true,      'required' => true, 'value' => $agendamento->diaHoraInicioCarbon->format('Y-m-d')];
        $campos[] = ['component' => INPUT_TEXT, 'label' => 'Horário',               'name' => 'hora',           'id' => 'hora',                'maxLength' => '5',      'disabled' => true,      'required' => true, 'value' => $agendamento->diaHoraInicioCarbon->format('H:i')];

    @endphp

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Agendamento',
        'subtitulo' => '',
        'campos'    => $campos,
        'btnSalvar' => false
    ])

    <script>
        $('document').ready(function() {
            var tempoEstimado = 0;
            var valorServico = 0.00;

            $('input[name^="servico"]:checked').each(function(i, e) {
                valorServico  = parseFloat(valorServico) + parseFloat( $(e).data('valor').replace('.', '').replace(',', '.') );
                tempoEstimado = parseInt(tempoEstimado) + parseInt( $(e).data('tempo_estimado') );
            });

            $('#valor').val(valorServico.toFixed(2)).trigger('input');
            $('#tempo_estimado').val(tempoEstimado).trigger('input');
        });
    </script>
@endsection
