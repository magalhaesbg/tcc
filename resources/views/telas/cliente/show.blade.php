@extends('layouts.app')
{{-- @TODO Magalhaes, Felipe - Tela precisa ser concluida, quando implementado outras funcionalidades no sistema, como o agendamento. --}}
@section('content')
    <div class="card">
        <div class="card-header">
            <div class='row'>
                <div class='col-md-6 row align-items-center'>
                    <h4 class='mb-0'>Cliente - {{ $cliente->name }} (CPF: {{ $cliente->cpf }})</h4>
                </div>
                <div class='col-md-6 row align-items-center'>
                    <div class="d-flex flex-row-reverse">
                        <a href="{{ url()->previous() }}" class="btn btn-secondary">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class='row'>
                                <div class='col-12 row align-items-center'>
                                    <h4 class='mb-0'>Histórico de agendamentos</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead class="table-dark">
                                  <tr>
                                    <th>Serviços</th>
                                    <th>Início</th>
                                    <th>Fim</th>
                                    <th>Status</th>
                                    <th>Resgatado</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cliente->agendamento as $agendamento)
                                        <tr>
                                            <td class="align-middle">
                                                <ul>
                                                    @foreach($agendamento->agendamentoServico as $servico)
                                                        <li>{{$servico->servico->nome}} - (R$ {{$servico->valor}})</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td class="align-middle">{{$agendamento->dia_hora_inicio}}</td>
                                            <td class="align-middle">{{$agendamento->dia_hora_fim}}</td>
                                            <td class="align-middle">
                                                @if ($agendamento->status === 'Concluído')
                                                    <span class="badge bg-success">Concluído</span>
                                                @endif
                                                @if ($agendamento->status === 'Agendado')
                                                    <span class="badge bg-primary">Agendado</span>
                                                @endif
                                                @if ($agendamento->status === 'Cancelado')
                                                    <span class="badge bg-danger">Cancelado</span>
                                                @endif
                                            </td>
                                            <td class="align-middle">
                                                @if (is_null($agendamento->fidelidade))
                                                    Não regatado
                                                @elseif (is_null($agendamento->fidelidade->data_resgate))
                                                    Não regatado
                                                @else
                                                    {{$agendamento->fidelidade->data_resgate->format('d/m/Y')}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
