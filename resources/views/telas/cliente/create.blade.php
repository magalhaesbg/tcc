@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Novo Cliente',
        'subtitulo' => '',
        'route'     => 'cliente.store',
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'name',           'id' => 'name',                      'required' => true, 'maxLength' => '140'],
            ['component' => CPF,        'label' => 'CPF',            'name' => 'cpf',            'id' => 'cpf',                       'required' => true, 'maxLength' => '14'],
            ['component' => INPUT_TEXT, 'label' => 'Email',          'name' => 'email',          'id' => 'email',                     'required' => true, 'maxLength' => '140'],
        ]
    ])

@endsection
