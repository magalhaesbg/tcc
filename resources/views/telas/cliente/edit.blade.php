@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'titulo'    => 'Editar Cliente',
        'route'     => 'cliente.update',
        'id'        => $cliente->id,
        'isEdit'    => true,
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'name',           'id' => 'name',       'value' =>  $cliente->name,       'required' => true, 'maxLength' => '140'],
            ['component' => CPF,        'label' => 'CPF',            'name' => 'cpf',            'id' => 'cpf',        'value' =>  $cliente->cpf,        'required' => true, 'maxLength' => '14'],
            ['component' => INPUT_TEXT, 'label' => 'Email',          'name' => 'email',          'id' => 'email',      'value' =>  $cliente->email,      'required' => true, 'maxLength' => '140'],
        ]
    ])
@endsection
