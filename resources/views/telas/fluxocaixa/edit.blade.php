@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'titulo'    => 'Editar Movimentação',
        'route'     => 'fluxocaixa.update',
        'id'        => $fluxoCaixa->id,
        'isEdit'    => true,
        'campos'        => [
            ['component' => DATE,       'label' => 'Data',                  'name' => 'data',           'id' => 'data',           'value' => $fluxoCaixa->data->format('Y-m-d'),    'required' => true],
            ['component' => INPUT_TEXT, 'label' => 'Descrição',             'name' => 'descricao',      'id' => 'descricao',      'value' => $fluxoCaixa->descricao,                'required' => false, 'maxLength' => '100'],
            ['component' => MONEY,      'label' => 'Valor',                 'name' => 'valor',          'id' => 'valor',          'value' => $fluxoCaixa->valor,                    'required' => true],
            ['component' => SELECT,     'label' => 'Tipo de Movimentação',  'name' => 'tipo_fluxo_id',  'id' => 'tipo_fluxo_id',  'value' => $fluxoCaixa->tipo_fluxo_id,            'required' => true, 'valores' => $tipoFluxo],
        ]
    ])
@endsection
