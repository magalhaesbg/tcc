@extends('layouts.app')
{{-- @TODO Magalhaes, Felipe - Tela precisa ser concluida, quando implementado outras funcionalidades no sistema, como o agendamento. --}}
@section('content')
    <div class="card">
        <div class="card-header">
            <div class='row'>
                <div class='col-md-6 row align-items-center'>
                    <h4 class='mb-0'>Cliente - {{ $cliente->name }} (CPF: {{ $cliente->cpf }})</h4>
                </div>
                <div class='col-md-6 row align-items-center'>
                    <div class="d-flex flex-row-reverse">
                        <a href="{{ url()->previous() }}" class="btn btn-secondary">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class='row'>
                                <div class='col-12 row align-items-center'>
                                    <h4 class='mb-0'>Histórico de agendamentos</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            a
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-1">
                    <div class="card">
                        <div class="card-header">
                            <div class='row'>
                                <div class='col-12 row align-items-center'>
                                    <h4 class='mb-0'>Fidelidade</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            a
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
