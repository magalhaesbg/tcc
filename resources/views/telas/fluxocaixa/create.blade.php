@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Nova Movimentação',
        'subtitulo' => '',
        'route'     => 'fluxocaixa.store',
        'campos'        => [
            ['component' => DATE,       'label' => 'Data',                  'name' => 'data',           'id' => 'data',                      'required' => true],
            ['component' => INPUT_TEXT, 'label' => 'Descrição',             'name' => 'descricao',      'id' => 'descricao',                 'required' => false, 'maxLength' => '100'],
            ['component' => MONEY,      'label' => 'Valor',                 'name' => 'valor',          'id' => 'valor',                     'required' => true],
            ['component' => SELECT,     'label' => 'Tipo de Movimentação',  'name' => 'tipo_fluxo_id',  'id' => 'tipo_fluxo_id',             'required' => true, 'valores' => $tipoFluxo],
        ]
    ])

@endsection
