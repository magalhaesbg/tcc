@extends('layouts.app')

@section('content')

<div class="content">
  <div class="container-fluid">
      <div class="row">

        @if (Auth::user()->hasRole(['Gerente']))

            @include('telas.dashboard.gerente.index')

        @elseif (Auth::user()->hasRole(['Barbeiro']))

            @include('telas.dashboard.barbeiro.index')

        @elseif (Auth::user()->hasRole(['Atendente']))

            @include('telas.dashboard.atendente.index')

        @elseif (Auth::user()->hasRole(['Cliente']))

            @include('telas.dashboard.cliente.index')

        @endif

      </div>
    </div>
</div>

@endsection
