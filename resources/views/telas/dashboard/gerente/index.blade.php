<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="alert alert-info" role="alert">
    <form method="GET" action="{{ route('dashboard') }}">
        @csrf
        <div class="row">
            <div class="col-2">
                @php
                    $mes = request()->mes ?? \Carbon\Carbon::now()->month;
                @endphp
                <select class="form-select" aria-label="Default select example" name="mes" required>
                    <option value="1"  @if($mes == 1) selected @endif>Janeiro</option>
                    <option value="2"  @if($mes == 2) selected @endif>Fevereiro</option>
                    <option value="3"  @if($mes == 3) selected @endif>Março</option>
                    <option value="4"  @if($mes == 4) selected @endif>Abril</option>
                    <option value="5"  @if($mes == 5) selected @endif>Maio</option>
                    <option value="6"  @if($mes == 6) selected @endif>Junho</option>
                    <option value="7"  @if($mes == 7) selected @endif>Julho</option>
                    <option value="8"  @if($mes == 8) selected @endif>Agosto</option>
                    <option value="9"  @if($mes == 9) selected @endif>Setembro</option>
                    <option value="10" @if($mes == 10) selected @endif>Outubro</option>
                    <option value="11" @if($mes == 11) selected @endif>Novembro</option>
                    <option value="12" @if($mes == 12) selected @endif>Dezembro</option>
                </select>
            </div>
            <div class="col-2">
                <input class="form-control" type='text' placeholder="{{\Carbon\Carbon::now()->year}}" pattern="20[0-9]{2}$" value="{{$ano ?? \Carbon\Carbon::now()->year}}" name="ano" required>
            </div>
            <div class="col-2 d-grid gap-2">
                <button class="btn btn-primary" type="submit">Filtrar</button>
            </div>
            <div class="col-6 d-flex flex-row-reverse" style="font-size: 10px">
                *Dados referentes do primeiro dia do mês até o dia atual
            </div>
        </div>
    </form>
</div>

<div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-header">
                Fluxo de Caixa (Progressão)
            </div>
            <div class="card-body">
                <div id="chart_fluxo_caixa"></div>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="card">
            <div class="card-header">
                Fluxo de Caixa
            </div>
            <div class="card-body">
                <div id="chart_fluxo_caixa_semanal"></div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-2">
    <div class="col-6">
        <div class="card">
            <div class="card-header">
                Atendimentos por Barbeiro
            </div>
            <div class="card-body">
                <div id="atendimentos_barbeiro"></div>
            </div>
        </div>
    </div>
    {{-- <div class="col-6">
        <div class="card">
            <div class="card-header">
                Serviços Prestados
            </div>
            <div class="card-body">
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            </div>
        </div>
    </div> --}}

    <div class="col-6">
        <div class="card">
            <div class="card-header">
                Ausência de Barbeiros
            </div>
            <div class="card-body">
                <div id="ausente_barbeiro"></div>
            </div>
        </div>
    </div>
</div>

@include('components.lineChart', [
    'divId'  => 'chart_fluxo_caixa',
    'titleH' => 'Semanas',
    'titleV' => 'Valor',
    'lines'  => [
        ['type' => 'number', 'value' => 'X'],
        ['type' => 'number', 'value' => 'Entrada'],
        ['type' => 'number', 'value' => 'Saída'],
    ],
    'colors' => ['#007329', '#AB0D06'],
    'trendlines' => [
    //     ['type' => 'linear', 'color' => '#111', 'opacity' => .5]
    ],
    'dadosGrafico' => $dadosFluxoCaixa
])

@include('components.lineChart', [
    'divId'  => 'chart_fluxo_caixa_semanal',
    'titleH' => 'Semanas',
    'titleV' => 'Valor',
    'lines'  => [
        ['type' => 'number', 'value' => 'X'],
        ['type' => 'number', 'value' => 'Entrada'],
        ['type' => 'number', 'value' => 'Saída'],
    ],
    'colors' => ['#007329', '#AB0D06'],
    'trendlines' => [
    //     ['type' => 'linear', 'color' => '#111', 'opacity' => .5]
    ],
    'dadosGrafico' => $dadosFluxoCaixaSemanal
])

@include('components.barChart', [
    'divId'  => 'atendimentos_barbeiro',
    'dadosGrafico' => $dadosBarbeiros
])

@include('components.barChart', [
    'divId'  => 'ausente_barbeiro',
    'dadosGrafico' => $dadosBarbeiroAusente
])
