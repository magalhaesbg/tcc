@extends('layouts.app')

@section('content')
    <div class="row mb-4">
        <div class="col-12">
            <div class="d-grid gap-2">
                <a href="{{route('agendamento.add')}}" class="btn btn-success btn-lg">
                    <i class="bi bi-plus-lg"></i>
                    Agendamento
                </a>
            </div>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-12">
            @if ($configFidelidade == $fidelidade->count())
                <div class="alert alert-success" role="alert">
                    Você pode solicitar sua fidelidade. {{$fidelidade->count()}}/{{$configFidelidade}}
                </div>
            @else
                <div class="alert alert-warning" role="alert">
                    Você ainda não pode solicitar sua fidelidade. {{$fidelidade->count()}}/{{$configFidelidade}}
                </div>
            @endif

        </div>
    </div>

    <div class="row">
        @include('telas.layout.mensagem')
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    Próximos Serviços
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Serviços</th>
                                <th>Barbeiro</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($agendamentos->isNotEmpty())
                                @foreach ($agendamentos as $agendamento)
                                    @php
                                        $destaqueHoraAtrasada = '';
                                        if ($agendamento->diaHoraInicioCarbon->lt(\Carbon\Carbon::now())) {
                                            $destaqueHoraAtrasada = 'text-danger';
                                        }
                                    @endphp
                                    <tr>
                                        <td class="align-middle">
                                            <ul>
                                                @foreach ($agendamento->agendamentoServico as $servico)
                                                    <li>
                                                        {{$servico->servico->nome}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td class="align-middle">{{$agendamento->barbeiro->name}}</td>
                                        <td class="align-middle {{$destaqueHoraAtrasada}}">{{$agendamento->dia_hora_inicio}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">
                                        Não há agendamentos
                                    </td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    Lista de serviços
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th class="text-center">Valor</th>
                                <th class="text-center">Tempo Estimado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($servicos AS $servico)
                                <tr>
                                    <td>{{ $servico->nome }}</td>
                                    <td class="text-center">R$ {{ $servico->valor }}</td>
                                    <td class="text-center">{{ $servico->tempo_estimado }} min</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
