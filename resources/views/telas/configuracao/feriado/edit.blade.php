@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Editar Feriado',
        'subtitulo' => '',
        'route'     => 'configuracao.feriado.update',
        'isEdit'    => true,
        'id'        => $feriado->id,
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'nome',            'id' => 'nome',               'required' => true, 'value' => $feriado->nome, 'maxLength' => '140'],
            ['component' => DATE,       'label' => 'Dia',            'name' => 'inicio',          'id' => 'inicio',             'required' => true, 'value' => $feriado->inicio->format('Y-m-d'), 'min' => date('Y-m-d')],
            ['component' => SELECT,     'label' => 'Tipo Feriado',   'name' => 'tipo_feriado_id', 'id' => 'tipo_feriado_id',    'required' => true, 'value' => $feriado->tipo_feriado_id, 'valores' => $tipoFeriado],
        ]
    ])

@endsection
