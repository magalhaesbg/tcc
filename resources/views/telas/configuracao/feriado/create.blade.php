@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Novo Feriado',
        'subtitulo' => '',
        'route'     => 'configuracao.feriado.store',
        'campos'        => [
            ['component' => INPUT_TEXT, 'label' => 'Nome',           'name' => 'nome',            'id' => 'nome',               'required' => true, 'maxLength' => '140'],
            ['component' => DATE,       'label' => 'Dia',            'name' => 'inicio',          'id' => 'inicio',             'required' => true, 'min' => date('Y-m-d')],
            ['component' => SELECT,     'label' => 'Tipo Feriado',   'name' => 'tipo_feriado_id', 'id' => 'tipo_feriado_id',    'required' => true, 'valores' => $tipoFeriado],
        ]
    ])

@endsection
