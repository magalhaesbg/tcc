@extends('layouts.app')

@section('head-js')
    <script src="{{ asset('js/jquery-dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables-responsive.min.js') }}"></script>
@endsection

@section('head-css')
    <link rel="stylesheet" href="{{ asset('css/jquery-dataTables.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/responsive-dataTable.min.css') }}" type="text/css">
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <div class='row'>
                <div class='col-6 row align-items-center'>
                    <h4 class='mb-0'>Feriados</h4>
                </div>
                <div class='col-6 d-flex flex-row-reverse'>
                    <a href="{{ route('configuracao.feriado.add') }}" class="btn btn-success">
                        <i class="bi bi-plus-lg"></i>
                        Feriado
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('telas.layout.mensagem')
            <table class="table table-hover data-table" style="width: 100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Dia</th>
                        <th>Descrição</th>
                        <th>Tipo Feriado</th>
                        <th width="150px"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            var table = $('.data-table').DataTable({
                oLanguage: {
                    sUrl: '../datatable/language.txt'
                },
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('configuracao.feriado') }}",
                columns: [
                    {data: 'id',     name: 'id'},
                    {data: 'dia',    name: 'dia'},
                    {data: 'nome',   name: 'nome'},
                    {data: 'tipo_feriado.nome',  name: 'tipo_feriado.nome'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                order: [
                    [1, 'asc']
                ]
            });

            window.onresize = function() {
                table.columns.adjust().draw();
            }
        });

    </script>
@endsection
