@php
    $domingo    = $parametros->horario->where('dia_semana_id', 1)->first();
    $segunda    = $parametros->horario->where('dia_semana_id', 2)->first();
    $terca      = $parametros->horario->where('dia_semana_id', 3)->first();
    $quarta     = $parametros->horario->where('dia_semana_id', 4)->first();
    $quinta     = $parametros->horario->where('dia_semana_id', 5)->first();
    $sexta      = $parametros->horario->where('dia_semana_id', 6)->first();
    $sabado     = $parametros->horario->where('dia_semana_id', 7)->first();
@endphp
<div class="row mt-2">
    <div class="col-12">
        <div class="alert alert-info" role="alert">
            <h4>Configuração do horário de atendimento</h4>
            - Dia não configurado é considerado como não trabalhado.<br>
            - Turno (manhã ou tarde) não preenchido é considerado como não trabalhado.
        </div>
        <div class="card mt-2">
            <div class="card-header">
                Domingo
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="domingoEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[1][entrada_manha]" class="form-control time" id="domingoEntradaManha" placeholder="00:00" value={{$domingo->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="domingoEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[1][saida_manha]" class="form-control time" id="domingoEntradaManha" placeholder="00:00" value={{$domingo->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="domingoEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[1][entrada_tarde]" class="form-control time" id="domingoEntradaManha" placeholder="00:00" value={{$domingo->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="domingoEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[1][saida_tarde]" class="form-control time" id="domingoEntradaManha" placeholder="00:00" value={{$domingo->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                Segunda-feira
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="segundaEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[2][entrada_manha]" class="form-control time" id="segundaEntradaManha" placeholder="00:00" value={{$segunda->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="segundaEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[2][saida_manha]" class="form-control time" id="segundaEntradaManha" placeholder="00:00" value={{$segunda->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="segundaEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[2][entrada_tarde]" class="form-control time" id="segundaEntradaManha" placeholder="00:00" value={{$segunda->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="segundaEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[2][saida_tarde]" class="form-control time" id="segundaEntradaManha" placeholder="00:00" value={{$segunda->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                Terça-feira
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="tercaEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[3][entrada_manha]" class="form-control time" id="tercaEntradaManha" placeholder="00:00" value={{$terca->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="tercaEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[3][saida_manha]" class="form-control time" id="tercaEntradaManha" placeholder="00:00" value={{$terca->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="tercaEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[3][entrada_tarde]" class="form-control time" id="tercaEntradaManha" placeholder="00:00" value={{$terca->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="tercaEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[3][saida_tarde]" class="form-control time" id="tercaEntradaManha" placeholder="00:00" value={{$terca->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                Quarta-feira
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quartaEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[4][entrada_manha]" class="form-control time" id="quartaEntradaManha" placeholder="00:00" value={{$quarta->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quartaEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[4][saida_manha]" class="form-control time" id="quartaEntradaManha" placeholder="00:00" value={{$quarta->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quartaEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[4][entrada_tarde]" class="form-control time" id="quartaEntradaManha" placeholder="00:00" value={{$quarta->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quartaEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[4][saida_tarde]" class="form-control time" id="quartaEntradaManha" placeholder="00:00" value={{$quarta->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                Quinta-feira
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quintaEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[5][entrada_manha]" class="form-control time" id="quintaEntradaManha" placeholder="00:00" value={{$quinta->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quintaEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[5][saida_manha]" class="form-control time" id="quintaEntradaManha" placeholder="00:00" value={{$quinta->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quintaEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[5][entrada_tarde]" class="form-control time" id="quintaEntradaManha" placeholder="00:00" value={{$quinta->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="quintaEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[5][saida_tarde]" class="form-control time" id="quintaEntradaManha" placeholder="00:00" value={{$quinta->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                Sexta-feira
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sextaEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[6][entrada_manha]" class="form-control time" id="sextaEntradaManha" placeholder="00:00" value={{$sexta->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sextaEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[6][saida_manha]" class="form-control time" id="sextaEntradaManha" placeholder="00:00" value={{$sexta->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sextaEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[6][entrada_tarde]" class="form-control time" id="sextaEntradaManha" placeholder="00:00" value={{$sexta->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sextaEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[6][saida_tarde]" class="form-control time" id="sextaEntradaManha" placeholder="00:00" value={{$sexta->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                Sábado
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sabadoEntradaManha">Entrada (manhã)</label>
                            <input type="text" name="horario[7][entrada_manha]" class="form-control time" id="sabadoEntradaManha" placeholder="00:00" placeholder="00:00" value={{$sabado->entrada_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sabadoEntradaManha">Saída (manhã)</label>
                            <input type="text" name="horario[7][saida_manha]" class="form-control time" id="sabadoEntradaManha" placeholder="00:00" placeholder="00:00" value={{$sabado->saida_manha ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sabadoEntradaManha">Entrada (Tarde)</label>
                            <input type="text" name="horario[7][entrada_tarde]" class="form-control time" id="sabadoEntradaManha" placeholder="00:00" placeholder="00:00" value={{$sabado->entrada_tarde ?? ''}}>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="sabadoEntradaManha">Saída (Tarde)</label>
                            <input type="text" name="horario[7][saida_tarde]" class="form-control time" id="sabadoEntradaManha" placeholder="00:00" placeholder="00:00" value={{$sabado->saida_tarde ?? ''}}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
