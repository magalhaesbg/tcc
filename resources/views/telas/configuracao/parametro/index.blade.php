@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @if (Session::get('mensagem') === false)
        <div class="alert alert-danger" role="alert">
            Erro ao alterar as configurações
        </div>
    @endif

    @if (Session::has('mensagem') && Session::get('mensagem') !== false)
        <div class="alert alert-success" role="alert">
            Configurações alteradas com sucesso.
        </div>
    @endif

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Parâmetros',
        'subtitulo' => '',
        'route'     => 'configuracao.parametro.update',
        'id'        => 1,
        'isEdit'      => true,
        'campos'        => [
            ['component' => INPUT_TEXT, 'label'  => 'Nome do Estabelecimento', 'name' => 'nome',             'id' => 'nome',         'required' => true, 'maxLength' => '100', 'value' => $parametros->nome],
            ['component' => CNPJ,       'label'  => 'CNPJ',                    'name' => 'cnpj',             'id' => 'cnpj',         'required' => true, 'value' => $parametros->cnpj],
            ['component' => INTEGER,    'label'  => 'Quantidade de Fidelidade','name' => 'numero_fidelidade','id' => 'fidelidade',   'required' => true, 'value' => $parametros->numero_fidelidade],
            ['component' => CUSTOM,     'custom' => 'telas.configuracao.parametro.horarios'],
        ]
    ])

@endsection
