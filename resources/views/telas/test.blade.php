@extends('layouts.app')

@section('content')

@include('layouts.defines')

@include('layouts.form', [
    'columns'   => 12,
    'titulo'    => 'Título - Página de teste dos componentes disponiveis.',
    'subtitulo' => 'Apresentação dos componentes implementados no sistema.',
    'route'     => '',
    'campos'        => [
        ['component' => INPUT_TEXT, 'label' => 'Campo INPUT_TEXT',  'name' => 'name_1', 'id' => 'id_1', 'required' => true, 'maxLength' => '10'],
        ['component' => DATE,       'label' => 'Campo DATE',        'name' => 'name_2', 'id' => 'id_2', 'required' => true, 'min' => '2022-02-25', 'max' => '2022-02-27'],
        ['component' => CPF,        'label' => 'Campo CPF',         'name' => 'name_3', 'id' => 'id_3', 'required' => true],
        ['component' => CNPJ,       'label' => 'Campo CNPJ',        'name' => 'name_4', 'id' => 'id_4', 'required' => true],
        ['component' => MONEY,      'label' => 'Campo MONEY',       'name' => 'name_5', 'id' => 'id_5', 'required' => true],
        ['component' => TIME,       'label' => 'Campo TIME',        'name' => 'name_6', 'id' => 'id_6', 'required' => true],
        ['component' => DATETIME,   'label' => 'Campo DATETIME',    'name' => 'name_7', 'id' => 'id_7', 'required' => true],
        ['component' => PHONE,      'label' => 'Campo PHONE',       'name' => 'name_8', 'id' => 'id_8', 'required' => true],
    ]
])

@stop
