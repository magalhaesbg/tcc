<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class='row'>
                    <div class='col-12 row align-items-center'>
                        <h4 class='mb-0'>{{ $titulo }}</h4>
                    </div>
                </div>
            </div>

            <div class="card-body">
                @if(isset($isEdit) && $isEdit)
                    @if (isset($route))
                        <form class="needs-validation" novalidate action="{{ route($route, $id) }}" method="POST">
                    @else
                        <form class="needs-validation" novalidate method="POST">
                    @endif

                        @csrf
                        @method('PUT')
                @else
                    @if (isset($route))
                        <form class="needs-validation" novalidate action="{{ route($route) }}" method="POST">
                    @else
                        <form class="needs-validation" novalidate method="POST">
                    @endif

                        @csrf
                @endif

                    <div class="row" id="campos-form">
                        @include('layouts.dynamics-form')
                    </div>

                    <div class="row">
                        <div class="col-sm-12 mt-3">
                            @if (!isset($btnSalvar) || (isset($btnSalvar) && $btnSalvar === true))
                                @if(isset($isEdit) && $isEdit)
                                    @include('components.button', [
                                        'id'    => 'btn_editar',
                                        'label' => 'Alterar',
                                        'class' => 'btn btn-warning',
                                        'style' => 'float: right;'
                                    ])
                                @else
                                    @include('components.button', [
                                        'id'    => 'btn_salvar',
                                        'label' => 'Adicionar',
                                        'class' => 'btn btn-success',
                                        'style' => 'float: right;'
                                    ])
                                @endif
                            @endif

                            <a href="{{ url()->previous() }}" class="btn btn-secondary" style="float: right;margin-right:.5rem">Voltar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    // setTimeout(function(){ $(".alert").remove(); }, 8000);
</script>
