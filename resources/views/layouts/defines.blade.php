@php
    define("INPUT_TEXT", "input_text");
    define("DATE", "date");
    define("CPF", "cpf");
    define("CNPJ", "cnpj");
    define("MONEY", "money");
    define("TIME", "time");
    define("DATETIME", "datetime");
    define("PHONE", "phone");
    define("INTEGER", "integer");
    define("PASSWORD", "password");
    define('SELECT', "select");
    define("CHECKBOX", "checkbox");
    define("CUSTOM", "custom");
@endphp
