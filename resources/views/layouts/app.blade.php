<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/jquery.js') }}" ></script>
        @yield('head-js')
        <script src="{{ asset('js/jquery-mask.js') }}" ></script>
        <script src="{{ asset('js/tcc.js') }}" ></script>

        <!-- Fonts -->
        <link href="{{ asset('fonts/google_nunito.css') }}" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('head-css')
        <link href="{{ asset('css/tcc.css') }}" rel="stylesheet">
    </head>
    <body id="body-pd">
        @include('telas.layout.menu')

        <div class="modal fade" id="alert_modal" tabindex="-1" aria-labelledby="alert_modal" aria-hidden="true" data-bs-backdrop="static">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="alert_modal">Atenção</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Confirmar exclusão/desativação?
                    </div>
                    <div id="modal-buttons" class="modal-footer">
                        {{-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button> --}}
                        {{-- <button id="confirmar" type="button" class="btn btn-primary">Confirmar</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
