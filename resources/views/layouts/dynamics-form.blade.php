@if (isset($campos) && !empty($campos))
    @foreach ($campos as $campo)

        @switch($campo['component'])
            @case('input_text')
                @include('components.input_text', $campo)
                @break

            @case('date')
                @include('components.date', $campo)
                @break

            @case('cpf')
                @include('components.cpf', $campo)
                @break

            @case('cnpj')
                @include('components.cnpj', $campo)
                @break

            @case('money')
                @include('components.money', $campo)
                @break

            @case('time')
                @include('components.time', $campo)
                @break

            @case('datetime')
                @include('components.datetime', $campo)
                @break

            @case('phone')
                @include('components.phone', $campo)
                @break

            @case('integer')
                @include('components.integer', $campo)
                @break

            @case('password')
                @include('components.password', $campo)
                @break

            @case('select')
                @include('components.select', $campo)
                @break

            @case('checkbox')
                @include('components.checkbox', $campo)
                @break

            @case('custom')
                @include('components.custom', $campo)
                @break
        @endswitch

    @endforeach
@endif
