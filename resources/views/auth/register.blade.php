<x-guest-layout>

    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <meta http-equiv="cache-control" content="no-cache" />
            <meta http-equiv="pragma" content="no-cache">
            <meta http-equiv="expires" content="0">

            <title>Barbearia</title>

            <!-- Scripts -->
            <script src="{{ asset('js/app.js') }}" defer></script>
            <script src="{{ asset('js/jquery.js') }}" ></script>
            <script src="{{ asset('js/jquery-mask.js') }}" ></script>
            <script src="{{ asset('js/tcc.js') }}" ></script>

            <!-- Fonts -->
            <link href="{{ asset('fonts/google_nunito.css') }}" rel="stylesheet">

            <!-- Styles -->
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            <link href="{{ asset('css/lg.css') }}" rel="stylesheet">

        </head>
        <body class="hold-transition image-custom">
            <section class="vh-100">
                <div class="container py-5 h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                            <div class="card bg-dark text-white" style="border-radius: 1rem;">
                                <form id="frm-login" action="{{ route('register') }}" method="POST">
                                    @csrf
                                    <div class="card-body p-5 text-center">

                                        <div class="mb-md-5 mt-md-4 pb-5">

                                            <h2 class="fw-bold mb-2 text-uppercase">Registrar</h2>
                                            <p class="text-white-50 mb-5">Preencha os campos abaixo.</p>

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="name">Nome</label>
                                                <input type="text" id="name" name="name" class="form-control form-control-lg" value="{{ old('name') }}"/>
                                                @error('name')
                                                    <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="name">CPF</label>
                                                <input type="text" id="cpf" name="cpf" class="form-control form-control-lg cpf" value="{{ old('cpf') }}"/>
                                                @error('cpf')
                                                    <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="email">Email</label>
                                                <input type="email" id="email" name="email" class="form-control form-control-lg" value="{{ old('email') }}"/>
                                                @error('email')
                                                    <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="password">Senha</label>
                                                <input type="password" id="password" name="password" class="form-control form-control-lg"/>
                                                @error('password')
                                                    <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="password_confirmation">Confirmação de Senha</label>
                                                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control form-control-lg" />
                                                @error('password_confirmation')
                                                    <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>

                                            <button class="btn btn-outline-light btn-lg px-5" type="submit">Cadastrar</button>
                                        </div>

                                        <div>
                                            <p class="mb-0">Já possui cadastro? <a href="{{route('login')}}" class="text-white-50 fw-bold">Entrar!</a></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <script>
            $('.cpf').mask('000.000.000-00', {reverse: true});
        </script>
        </body>
    </html>
</x-guest-layout>
