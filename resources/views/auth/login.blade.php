<x-guest-layout>

    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <meta http-equiv="cache-control" content="no-cache" />
            <meta http-equiv="pragma" content="no-cache">
            <meta http-equiv="expires" content="0">

            <title>Barbearia</title>

                <!-- Scripts -->
                <script src="{{ asset('js/app.js') }}" defer></script>
                <script src="{{ asset('js/jquery.js') }}" ></script>
                <script src="{{ asset('js/jquery-mask.js') }}" ></script>
                <script src="{{ asset('js/tcc.js') }}" ></script>

                <!-- Fonts -->
                <link href="{{ asset('fonts/google_nunito.css') }}" rel="stylesheet">

                <!-- Styles -->
                <link href="{{ asset('css/app.css') }}" rel="stylesheet">
                <link href="{{ asset('css/lg.css') }}" rel="stylesheet">

        </head>
        <body class="hold-transition image-custom">
            <section class="vh-100">
                <div class="container py-5 h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                            <div class="card bg-dark text-white" style="border-radius: 1rem;">
                                <form id="frm-login" action="{{ route('login') }}" method="POST">
                                    @csrf
                                    <div class="card-body p-5 text-center">

                                        <div class="mb-md-2 mt-md-4 pb-5">

                                            <h2 class="fw-bold mb-2 text-uppercase">Entrar</h2>
                                            <p class="text-white-50 mb-5">Por favor, entre com suas credenciais.</p>
                                            @error('email')
                                                <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                            @error('password')
                                                <div class="alert alert-warning pb-1 pt-1" role="alert">
                                                    {{ $message }}
                                                </div>
                                            @enderror

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="email">Email</label>
                                                <input type="email" id="email" name="email" class="form-control form-control-lg" />
                                            </div>

                                            <div class="form-outline form-white mb-4">
                                                <label class="form-label" for="password">Senha</label>
                                                <input type="password" id="password" name="password" class="form-control form-control-lg" />

                                            </div>

                                            <p class="small mb-5 pb-lg-2"><a class="text-white-50" href="#!">Esqueceu a senha?</a></p>

                                            <button id="entrar" class="btn btn-outline-light btn-lg px-5" type="submit">Entrar</button>

                                            <div class="row d-flex justify-content-center text-center mt-4 pt-1">
                                                <div class="row">
                                                    <div class="col-6 d-flex flex-row-reverse">
                                                        Entrar:
                                                    </div>
                                                    <div class="col-6 d-flex justify-content-left">
                                                        <a href="{{ route('auth.google') }}" class="text-white"><i class="bi bi-google"></i></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div>
                                            <p class="mb-0">Você não tem cadastro? <a href="{{route('register')}}" class="text-white-50 fw-bold">Cadastrar-se!</a></p>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </body>
    </html>
</x-guest-layout>
