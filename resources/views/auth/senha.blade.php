@extends('layouts.app')

@section('content')

    @include('layouts.defines')

    @include('layouts.form', [
        'columns'   => 12,
        'titulo'    => 'Troca de senha',
        'subtitulo' => '',
        'route'     => 'funcionario.updateSenha',
        'id'        => '',
        'isEdit'    => true,
        'campos'        => [
            ['component' => PASSWORD, 'label' => 'Senha',           'name' => 'password',               'id' => 'password',                 'required' => true, 'maxLength' => '30'],
            ['component' => PASSWORD, 'label' => 'Confirmar senha', 'name' => 'password_confirmation',  'id' => 'password_confirmation',    'required' => true, 'maxLength' => '30'],
        ]
    ])

@endsection
