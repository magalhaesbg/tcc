@php
    $classe = Arr::has($campo, 'class') ? $campo['class'] : '';
    $value  = $campo['value'] ?? collect();
    $count = 0;
@endphp

<div class="col-md-12">
    <fieldset class="border rounded-3 p-3">
        <legend class="float-none w-auto px-3">
            {{ $campo['label'] }}
        </legend>

        @error($campo['name'])
            <div class="alert alert-danger pb-0 pt-0 mt-1">{{ $message }}</div>
        @enderror

        @foreach ($campo['valores'] AS $valor)
            @if (($count % 3) == 0 && $count !== 0)
                </div>
            @endif
            @if (($count % 3) == 0)
                <div class="row">
            @endif
                <div class="col-md-4">
                    <div class="form-check">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            value="true"
                            name="{{$campo['name']}}[{{$valor->id}}]"
                            id="{{$campo['name']}}_{{$valor->id}}"
                            @checked(array_search($valor->id, $value->toArray()) !== false)
                            @if (Arr::has($campo, 'disabled') && $campo['disabled'] === true) disabled @endif

                            @if (Arr::has($campo, 'data'))
                                @foreach ($campo['data'] AS $dados)
                                    @if (eval( 'return $valor->'.$dados.';' ) !== NULL)
                                        data-{{$dados}}={{ eval( 'return $valor->'.$dados.';' ) }}
                                    @endif
                                @endforeach
                            @endif
                        >
                        <label class="form-check-label" for="{{$campo['name']}}_{{$valor->id}}">
                            {{ $valor->nome }}
                        </label>
                    </div>
                </div>
            @php
                $count ++;
            @endphp
        @endforeach
    </fieldset>
</div>
