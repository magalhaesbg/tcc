<script>
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawTrendlines);

    function drawTrendlines() {
        var data = new google.visualization.arrayToDataTable( {{Js::from($dadosGrafico)}});
        var trendlines = @json($trendlines);

        var options = {
            hAxis: {
                title: '{{$titleH}}'
            },
            vAxis: {
                title: '{{$titleV}}'
            },
            colors: @json($colors),

        };

        if (trendlines) {
            options.trendlines = trendlines;
        }

        var chart = new google.visualization.LineChart(document.getElementById('{{$divId}}'));
        chart.draw(data, options);
    }
</script>
