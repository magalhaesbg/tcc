<button
    type="submit"
    class="{{ $class }}"
    name="{{ $id }}"
    id="{{ $id }}"
    style="{{ $style }}"
>
    {{ $label }}
</button>
