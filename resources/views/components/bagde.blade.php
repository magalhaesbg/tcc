@switch($codigoBadge)
    @case('primary')
        <span class="badge bg-primary">{{ $mensagemBadge }}</span>
        @break

    @case('secondary')
        <span class="badge bg-secondary">{{ $mensagemBadge }}</span>
        @break

    @case('success')
        <span class="badge bg-success">{{ $mensagemBadge }}</span>
        @break

    @case('danger')
        <span class="badge bg-danger">{{ $mensagemBadge }}</span>
        @break

    @default
        PASSE OS PARAMETROS PARA O BADGE

@endswitch

