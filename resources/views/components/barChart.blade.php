<script>
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.arrayToDataTable({{Js::from($dadosGrafico)}});

        var view = new google.visualization.DataView(data);
        view.setColumns([
            0,
            1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2
        ]);


        var options = {
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };


        var chart = new google.visualization.BarChart(document.getElementById("{{$divId}}"));
        chart.draw(view, options);
    }
</script>
