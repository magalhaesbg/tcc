@php
    $classe = Arr::has($campo, 'class') ? $campo['class'] : '';
    $valor  = old($campo['name']) ??  $campo['value'] ?? '';
@endphp

<div class="col-md-12">
    <label for="{{ $campo['id'] }}" class="form-label">{{ $campo['label'] }}</label>
    <input
        type        = "date"
        class       = "form-control {{ $classe }}"
        id          = "{{ $campo['id'] }}"
        name        = "{{ $campo['name'] }}"
        value       = "{{ $valor }}"

        @if (Arr::has($campo, 'min')) min = "{{$campo['min']}}" @endif
        @if (Arr::has($campo, 'max')) max = "{{$campo['max']}}" @endif

        @if (Arr::has($campo, 'readonly') && $campo['readonly'] === true)      readonly @endif
        @if (Arr::has($campo, 'disabled') && $campo['disabled'] === true)      disabled @endif
        @if (Arr::has($campo, 'required') && $campo['required'] === true)      required @endif
    >
    <div class="invalid-feedback">
        @include('mensagem.erro_validacao_form')
    </div>
    @error($campo['name'])
        <div class="alert alert-danger pb-0 pt-0 mt-1">{{ $message }}</div>
    @enderror
</div>
