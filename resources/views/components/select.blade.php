@php
    $classe = Arr::has($campo, 'class') ? $campo['class'] : '';
    $value  = old($campo['name']) ?? $campo['value'] ?? '';
@endphp

<div class="col-md-12">
    <label for="{{ $campo['id'] }}" class="form-label">{{ $campo['label'] }}</label>
    <select
        class   = "form-select {{ $classe }}"
        id          = "{{ $campo['id'] }}"
        name        = "{{ $campo['name'] }}"

        @if (Arr::has($campo, 'readonly') && $campo['readonly'] === true)      readonly @endif
        @if (Arr::has($campo, 'disabled') && $campo['disabled'] === true)      disabled @endif
        @if (Arr::has($campo, 'required') && $campo['required'] === true)      required @endif
    >
        <option value="">Selecione...</option>
        @foreach ($campo['valores'] AS $valor)
            @if (Arr::has($campo, 'descricao_option'))
                <option value="{{ $valor->id }}" @selected(old($campo['name']) == $valor->id || $value == $valor->id)>{{ eval('return $valor->'.$campo['descricao_option'].';') }}</option>
            @else
                <option value="{{ $valor->id }}" @selected(old($campo['name']) == $valor->id || $value == $valor->id)>{{ $valor->name ?? $valor->nome ?? $valor->descricao }}</option>
            @endif
        @endforeach
    </select>
    <div class="invalid-feedback">
        @include('mensagem.erro_validacao_form')
    </div>
    @error($campo['name'])
        <div class="alert alert-danger pb-0 pt-0 mt-1">{{ $message }}</div>
    @enderror
</div>
