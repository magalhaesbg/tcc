<?php

use App\Http\Controllers\AgendamentoController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ConfiguracaoController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FluxoCaixaController;
use App\Http\Controllers\FuncionarioController;
use App\Http\Controllers\ServicoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/componentes', function () {
    return view('telas.test');
});

Route::middleware(['auth:sanctum', 'verified', 'UserAtivo', 'TrocaSenha'])->group(function () {
    // DASHBOARD
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // SERVICOS
    Route::get('/servico',              [ServicoController::class, 'index'])           ->middleware(['role:Gerente'])  ->name('servico.index');
    Route::get('/servico/add',          [ServicoController::class, 'create'])          ->middleware(['role:Gerente'])  ->name('servico.add');
    Route::post('/servico/store',       [ServicoController::class, 'store'])           ->middleware(['role:Gerente'])  ->name('servico.store');
    Route::get('/servico/edit/{id}',    [ServicoController::class, 'edit'])            ->middleware(['role:Gerente'])  ->name('servico.edit');
    Route::put('/servico/update/{id}',  [ServicoController::class, 'update'])          ->middleware(['role:Gerente'])  ->name('servico.update');
    Route::get('/servico/disable/{id}', [ServicoController::class, 'disable'])         ->middleware(['role:Gerente'])  ->name('servico.disable');

    // FUNCIONARIOS
    Route::get('/funcionario',               [FuncionarioController::class, 'index'])                ->middleware(['role:Gerente'])                              ->name('funcionario.index');
    Route::get('/funcionario/add',           [FuncionarioController::class, 'create'])               ->middleware(['role:Gerente'])                              ->name('funcionario.add');
    Route::post('/funcionario/store',        [FuncionarioController::class, 'store'])                ->middleware(['role:Gerente'])                              ->name('funcionario.store');
    Route::get('/funcionario/edit/{id}',     [FuncionarioController::class, 'edit'])                 ->middleware(['role:Gerente'])                              ->name('funcionario.edit');
    Route::put('/funcionario/update/{id}',   [FuncionarioController::class, 'update'])               ->middleware(['role:Gerente'])                              ->name('funcionario.update');
    Route::get('/funcionario/disable/{id}',  [FuncionarioController::class, 'disable'])              ->middleware(['role:Gerente'])                              ->name('funcionario.disable');
    Route::get('/funcionario/enable/{id}',   [FuncionarioController::class, 'enable'])               ->middleware(['role:Gerente'])                              ->name('funcionario.enable');
    Route::post('/funcionario/servico',      [FuncionarioController::class, 'getServicosBarbeiro'])  ->middleware(['role:Gerente|Barbeiro|Atendente|Cliente'])   ->name('funcionario.servico');

    // CLIENTES
    Route::get('/cliente',              [ClienteController::class, 'index'])       ->middleware(['role:Gerente|Atendente|Barbeiro'])  ->name('cliente.index');
    Route::get('/cliente/show/{id}',    [ClienteController::class, 'show'])        ->middleware(['role:Gerente|Atendente|Barbeiro'])  ->name('cliente.show');
    Route::get('/cliente/add',          [ClienteController::class, 'create'])      ->middleware(['role:Gerente|Atendente|Barbeiro'])  ->name('cliente.add');
    Route::post('/cliente/store',       [ClienteController::class, 'store'])       ->middleware(['role:Gerente|Atendente|Barbeiro'])  ->name('cliente.store');
    Route::get('/cliente/edit/{id}',    [ClienteController::class, 'edit'])        ->middleware(['role:Gerente|Atendente|Barbeiro'])  ->name('cliente.edit');
    Route::put('/cliente/update/{id}',  [ClienteController::class, 'update'])      ->middleware(['role:Gerente|Atendente|Barbeiro'])  ->name('cliente.update');

    // FLUXO CAIXA
    Route::get('/fluxo_caixa',              [FluxoCaixaController::class, 'index'])       ->middleware(['role:Gerente'])  ->name('fluxocaixa.index');
    Route::get('/fluxo_caixa/add',          [FluxoCaixaController::class, 'create'])      ->middleware(['role:Gerente'])  ->name('fluxocaixa.add');
    Route::post('/fluxo_caixa/store',       [FluxoCaixaController::class, 'store'])       ->middleware(['role:Gerente'])  ->name('fluxocaixa.store');
    Route::get('/fluxo_caixa/edit/{id}',    [FluxoCaixaController::class, 'edit'])        ->middleware(['role:Gerente'])  ->name('fluxocaixa.edit');
    Route::put('/fluxo_caixa/update/{id}',  [FluxoCaixaController::class, 'update'])      ->middleware(['role:Gerente'])  ->name('fluxocaixa.update');
    Route::get('/fluxo_caixa/update/{id}',  [FluxoCaixaController::class, 'delete'])      ->middleware(['role:Gerente'])  ->name('fluxocaixa.delete');

    // AGENDAMENTOS
    Route::get('/agendamento',                  [AgendamentoController::class, 'index'])        ->middleware(['role:Gerente|Atendente|Barbeiro|Cliente'])    ->name('agendamento.index');
    Route::get('/agendamento/show/{id}',        [AgendamentoController::class, 'show'])         ->middleware(['role:Gerente|Atendente|Barbeiro'])            ->name('agendamento.show');
    Route::get('/agendamento/add',              [AgendamentoController::class, 'create'])       ->middleware(['role:Gerente|Atendente|Barbeiro|Cliente'])    ->name('agendamento.add');
    Route::post('/agendamento/store',           [AgendamentoController::class, 'store'])        ->middleware(['role:Gerente|Atendente|Barbeiro|Cliente'])    ->name('agendamento.store');
    Route::get('/agendamento/edit/{id}',        [AgendamentoController::class, 'edit'])         ->middleware(['role:Gerente|Atendente|Barbeiro'])            ->name('agendamento.edit');
    Route::put('/agendamento/update/{id}',      [AgendamentoController::class, 'update'])       ->middleware(['role:Gerente|Atendente|Barbeiro'])            ->name('agendamento.update');
    Route::get('/agendamento/delete/{id}',      [AgendamentoController::class, 'delete'])       ->middleware(['role:Gerente|Atendente|Barbeiro'])            ->name('agendamento.delete');
    Route::post('/agendamento/horario',         [AgendamentoController::class, 'horario'])      ->middleware(['role:Gerente|Atendente|Barbeiro|Cliente'])    ->name('agendamento.horario');
    Route::get('/agendamento/concluido/{id}',   [AgendamentoController::class, 'concluido'])    ->middleware(['role:Gerente|Atendente|Barbeiro'])            ->name('agendamento.concluido');
    Route::get('/agendamento/cancelado/{id}',   [AgendamentoController::class, 'cancelado'])    ->middleware(['role:Gerente|Atendente|Barbeiro'])            ->name('agendamento.cancelado');

    // CONFIGURAÇÕES
    Route::get('/configuracao/parametro',           [ConfiguracaoController::class, 'parametro'])      ->middleware(['role:Gerente'])    ->name('configuracao.parametro');
    Route::put('/configuracao/parametro/edit/{id}', [ConfiguracaoController::class, 'update'])         ->middleware(['role:Gerente'])    ->name('configuracao.parametro.update');
    Route::get('/configuracao/feriado',             [ConfiguracaoController::class, 'feriado'])        ->middleware(['role:Gerente'])    ->name('configuracao.feriado');
    Route::get('/configuracao/feriado/add',         [ConfiguracaoController::class, 'create'])         ->middleware(['role:Gerente'])    ->name('configuracao.feriado.add');
    Route::post('/configuracao/feriado/store',      [ConfiguracaoController::class, 'store'])          ->middleware(['role:Gerente'])    ->name('configuracao.feriado.store');
    Route::get('/configuracao/feriado/edit/{id}',   [ConfiguracaoController::class, 'edit'])           ->middleware(['role:Gerente'])    ->name('configuracao.feriado.edit');
    Route::put('/configuracao/feriado/update/{id}', [ConfiguracaoController::class, 'updateFeriado'])  ->middleware(['role:Gerente'])    ->name('configuracao.feriado.update');
    Route::get('/configuracao/feriado/delete/{id}', [ConfiguracaoController::class, 'delete'])         ->middleware(['role:Gerente'])    ->name('configuracao.feriado.delete');

});

Route::middleware(['auth:sanctum', 'verified', 'UserAtivo'])->group(function () {
    // FUNCIONARIOS
    Route::get('/funcionario/senha',        [FuncionarioController::class, 'senha'])        ->name('funcionario.senha');
    Route::put('/funcionario/update_senha', [FuncionarioController::class, 'updateSenha'])  ->name('funcionario.updateSenha');
});

Route::get('/redirect', [LoginController::class, 'redirectToProvider'])->name('auth.google');
Route::get('/callback', [LoginController::class, 'handleProviderCallback']);

Auth::routes();
