<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// TESTE SOFTNEX
Route::post('/softnex', function() {
    \Illuminate\Support\Facades\Log::info('Chegou na rota' );
    \Illuminate\Support\Facades\Log::info('_SERVER:'.json_encode($_SERVER) );
    \Illuminate\Support\Facades\Log::info('SSL_CLIENT_VERIFY:'.Request()->server->get('SSL_CLIENT_VERIFY') );
    \Illuminate\Support\Facades\Log::info('SSL_CLIENT_S_DN:'.Request()->server->get('SSL_CLIENT_S_DN') );
    \Illuminate\Support\Facades\Log::info('Request -> SERVER:'.json_encode(Request()->server()) );
    \Illuminate\Support\Facades\Log::info('Request:'.json_encode(Request()) );

    return  json_encode([
        'teste' => 'ola'
    ]);
});
