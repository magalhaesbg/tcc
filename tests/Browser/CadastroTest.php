<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome;
use Tests\DuskTestCase;

class CadastroTest extends DuskTestCase
{
    public function testCadastroSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->press('a.fw-bold')
                    ->type('#name', 'Novo usuario')
                    ->type('#cpf', '405.309.220-53')
                    ->type('#email', 'novo_usuario@teste.com.br')
                    ->type('#password', '12345678')
                    ->type('#password_confirmation', '12345678')
                    ->press('.btn')
                    ->assertPathIs('/php80/tcc/dashboard');
        });
    }

    public function testLogout()
    {
        $this->browse(function (Browser $browser) {
            $browser->click('.nav > a:nth-child(2)')
                    ->assertPathIs('/php80/tcc/');
        });
    }

    public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('#email', 'novo_usuario@teste.com.br')
                    ->type('#password', '12345678')
                    ->press('#entrar')
                    ->assertPathIs('/php80/tcc/dashboard');
        });

        User::where('email', 'novo_usuario@teste.com.br')->delete();
    }
}
