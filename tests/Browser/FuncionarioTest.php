<?php

namespace Tests\Browser;

use App\Models\User;
use App\Models\UserServico;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FuncionarioTest extends DuskTestCase
{
    public function testLoginSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('#email', 'magalhaesbg@felipemagalhaes.com')
                    ->type('#password', '12345678')
                    ->press('#entrar')
                    ->assertPathIs('/php80/tcc/dashboard');
        });
    }

    public function testCreateFuncionario()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/funcionario')
                    ->click('a.btn-success:nth-child(1)')
                    ->type('name', 'Teste usuario')
                    ->type('cpf', '754.632.950-77')
                    ->type('email', 'teste_usuario@teste.com.br')
                    ->select('#role', '2')
                    ->pause(1000)
                    ->check('#servicos_1')
                    ->check('#servicos_3')
                    ->press('#btn_salvar')
                    ->pause(1000)
                    ->assertSee('Funcionário inserido com sucesso.');
        });
    }

    public function testEditFuncionario()
    {
        $user = User::where('email', 'teste_usuario@teste.com.br')->get()->first();

        $this->browse(function (Browser $browser) use($user){
            $browser->visit('/funcionario/edit/'.$user->id)
                    ->type('name', 'Teste Usuario Editado')
                    ->type('cpf', '754.632.950-77')
                    ->type('email', 'teste_usuario@teste.com.br')
                    ->press('#btn_editar')
                    ->assertSee('Funcionário alterado com sucesso.');
        });
    }

    public function testDisableFuncionario()
    {
        $user = User::where('email', 'teste_usuario@teste.com.br')->get()->first();

        $this->browse(function (Browser $browser) use($user) {
            $browser->visit('/funcionario/disable/'.$user->id)
                    ->pause(1000)
                    ->assertSee('Funcionário desativado com sucesso.');
        });
    }

    public function testEnableFuncionario()
    {
        $user = User::where('email', 'teste_usuario@teste.com.br')->get()->first();

        $this->browse(function (Browser $browser) use($user){
            $browser->visit('/funcionario/enable/'.$user->id)
                    ->pause(1000)
                    ->assertSee('Funcionário ativado com sucesso.');
        });

        UserServico::where('user_id', $user->id)->delete();
        User::where('email', 'teste_usuario@teste.com.br')->delete();
    }
}
