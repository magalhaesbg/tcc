<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    public function testLoginSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('#email', 'magalhaesbg@felipemagalhaes.com')
                    ->type('#password', '12345678')
                    ->press('#entrar')
                    ->assertPathIs('/php80/tcc/dashboard');
        });
    }

    public function testLogout()
    {
        $this->browse(function (Browser $browser) {
            $browser->click('.nav > a:nth-child(2)')
                    ->assertPathIs('/php80/tcc/');
        });
    }

    public function testLoginFailed()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('#email', 'magalhaesbg@felipemagalhaes.com')
                    ->type('#password', '00000000')
                    ->press('#entrar')
                    ->assertSee('Essas credenciais não foram encontradas em nossos registros.');
        });
    }
}
