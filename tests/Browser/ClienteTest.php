<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ClienteTest extends DuskTestCase
{
    public function testLoginSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('#email', 'magalhaesbg@felipemagalhaes.com')
                    ->type('#password', '12345678')
                    ->press('#entrar')
                    ->assertPathIs('/php80/tcc/dashboard');
        });
    }

    public function testCreateCliente()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/cliente')
                    ->click('.btn-success')
                    ->type('name', 'Fernando')
                    ->type('cpf', '528.668.060-77')
                    ->type('email', 'fernando@teste.com.br')
                    ->press('#btn_salvar')
                    ->pause(1000)
                    ->assertSee('Cliente inserido com sucesso.');
        });
    }

    public function testEditCliente()
    {
        $user = User::where('email', 'fernando@teste.com.br')->get()->first();

        $this->browse(function (Browser $browser) use($user){
            $browser->visit('/cliente/edit/'.$user->id)
                    ->type('name', 'Paulo Editado')
                    ->type('cpf', '528.668.060-77')
                    ->type('email', 'fernando@teste.com.br')
                    ->press('#btn_editar')
                    ->assertSee('Cliente alterado com sucesso.');
        });
    }

    public function testShowCliente()
    {
        $user = User::where('email', 'fernando@teste.com.br')->get()->first();

        $this->browse(function (Browser $browser) use($user){
            $browser->visit('/cliente/show/'.$user->id)
                    ->pause(500)
                    ->press('.btn')
                    ->assertPathIs('/php80/tcc/cliente');
        });

        User::where('email', 'fernando@teste.com.br')->delete();
    }
}
