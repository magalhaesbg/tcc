<?php

namespace Tests\Browser;

use App\Models\Servico;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ServicoTest extends DuskTestCase
{
    public function testLoginSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('#email', 'magalhaesbg@felipemagalhaes.com')
                    ->type('#password', '12345678')
                    ->press('#entrar')
                    ->assertPathIs('/php80/tcc/dashboard');
        });
    }

    public function testCreateServico()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/servico')
                    ->click('.btn-success')
                    ->type('nome', 'Sombrancelhas')
                    ->type('valor', '10,00')
                    ->type('tempo_estimado', '12')
                    ->press('#btn_salvar')
                    ->assertSee('Serviço inserido com sucesso.');
        });
    }

    public function testEditServico()
    {
        $servico = Servico::where('nome', 'Sombrancelhas')->where('valor', '10.00')->get()->first();

        $this->browse(function (Browser $browser) use($servico) {
            $browser->visit('/servico/edit/'.$servico->id)
                    ->type('nome', 'Sombrancelhas Editado')
                    ->type('valor', '12,00')
                    ->type('tempo_estimado', '10')
                    ->press('#btn_editar')
                    ->assertSee('Serviço alterado com sucesso.');
        });
    }

    public function testDisableServico()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/servico')
                    ->waitFor('tr.odd:nth-child(1) > td:nth-child(5) > button:nth-child(2)', 2)
                    ->press('tr.odd:nth-child(1) > td:nth-child(5) > button:nth-child(2)')
                    ->waitFor('a.btn:nth-child(2)', 2)
                    ->press('a.btn:nth-child(2)')
                    ->pause(1000)
                    ->assertSee('Serviço desativado com sucesso.');
        });

        Servico::where('nome', 'Sombrancelhas')->where('valor', '12.00')->delete();

    }
}
