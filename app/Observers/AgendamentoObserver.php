<?php

namespace App\Observers;

use App\Models\Agendamento;
use App\Services\FidelidadeService;
use App\Services\FluxoCaixaService;

class AgendamentoObserver
{
    public function __construct(
        FluxoCaixaService $fluxoCaixaService,
        FidelidadeService $fidelidadeService,
    ) {
        $this->fluxoCaixaService = $fluxoCaixaService;
        $this->fidelidadeService = $fidelidadeService;
    }

    /**
     * Handle the Agendamento "created" event.
     *
     * @param  \App\Models\Agendamento  $agendamento
     * @return void
     */
    public function created(Agendamento $agendamento)
    {

    }

    /**
     * Handle the Agendamento "updated" event.
     *
     * @param  \App\Models\Agendamento  $agendamento
     * @return void
     */
    public function updated(Agendamento $agendamento)
    {
        if ($agendamento->status === 'Concluído') {
            # Tratamento para o fluxo de caixa
            $valorAgendamento = 0.00;

            foreach($agendamento->agendamentoServico as $servico) {
                $valorAgendamento += $servico->valorDatabase;
            }

            $arDados = [
                'data'              => $agendamento->diaHoraInicioCarbon->format('Y-m-d H:i:s'),
                'descricao'         => 'Agendamento atendido',
                'valor'             => $valorAgendamento,
                'agendamento_id'    => $agendamento->id,
                'tipo_fluxo_id'     => 1
            ];

            request()->merge($arDados);

            $this->fluxoCaixaService->store(request());
        }
    }

    /**
     * Handle the Agendamento "deleted" event.
     *
     * @param  \App\Models\Agendamento  $agendamento
     * @return void
     */
    public function deleted(Agendamento $agendamento)
    {
        //
    }

    /**
     * Handle the Agendamento "restored" event.
     *
     * @param  \App\Models\Agendamento  $agendamento
     * @return void
     */
    public function restored(Agendamento $agendamento)
    {
        //
    }

    /**
     * Handle the Agendamento "force deleted" event.
     *
     * @param  \App\Models\Agendamento  $agendamento
     * @return void
     */
    public function forceDeleted(Agendamento $agendamento)
    {
        //
    }
}
