<?php

namespace App\Models;

use App\Casts\Dinheiro;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class AgendamentoServico extends Model
{
    use HasFactory;

    protected $casts = [
        'valor' => Dinheiro::class
    ];

    protected $fillable = [
        'valor',
        'agendamento_id',
        'servico_id',
    ];

    protected function valorDatabase(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['valor'],
        );
    }

    public function servico(): BelongsTo
    {
        return $this->belongsTo(Servico::class);
    }

    public function agendamento(): HasOne
    {
        return $this->hasOne(Agendamento::class);
    }
}
