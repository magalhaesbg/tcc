<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Horario extends Model
{
    use HasFactory;

    protected $fillable = [
        'parametro_id',
        'dia_semana_id',
        'entrada_manha',
        'saida_manha',
        'entrada_tarde',
        'saida_tarde',
    ];

    public function parametro(): BelongsTo
    {
        return $this->belongsTo(Parametro::class);
    }

    public function diaSemana(): BelongsTo
    {
        return $this->belongsTo(DiaSemana::class);
    }
}
