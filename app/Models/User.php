<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'cpf',
        'email',
        'password',
        'changed_password',
        'ativo'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $castss = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    protected function cpf(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => substr($value, 0, 3) . '.' . substr($value, 3, 3) . '.' . substr($value, 6, 3) . '.' . substr($value, 9, 2),
            set: fn ($value) => str_replace(['.', '-'], ['', ''], $value),
        );
    }

    protected function nameEmail(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['name'].' ('.$attributes['email'].')',
        );
    }

    public function ausente(): HasMany
    {
        return $this->hasMany(Ausente::class);
    }

    public function agendamento(): HasMany
    {
        return $this->hasMany(Agendamento::class);
    }

    public function userServico(): HasMany
    {
        return $this->hasMany(UserServico::class);
    }
}
