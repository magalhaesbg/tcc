<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TipoFluxo extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome'
    ];

    public function fluxoCaixa(): HasMany
    {
        return $this->hasMany(FluxoCaixa::class);
    }
}
