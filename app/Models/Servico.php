<?php

namespace App\Models;

use App\Casts\Dinheiro;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Servico extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
        'valor',
        'tempo_estimado',
        'ativo'
    ];

    protected $casts = [
        'valor' => Dinheiro::class
    ];

    public function userServico(): HasMany
    {
        return $this->hasMany(UserServico::class);
    }

    public function agendamentoServico(): HasMany
    {
        return $this->hasMany(AgendamentoServico::class);
    }
}
