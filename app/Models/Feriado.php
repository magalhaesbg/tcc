<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feriado extends Model
{
    use HasFactory;

    protected $dates = [
        'inicio',
        'fim'
    ];

    protected $fillable = [
        'nome',
        'inicio',
        'fim',
        'tipo_feriado_id'
    ];

    public function tipoFeriado(): BelongsTo
    {
        return $this->belongsTo(TipoFeriado::class);
    }
}
