<?php

namespace App\Models;

use App\Casts\Cnpj;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Parametro extends Model
{
    use HasFactory;

    protected $casts = [
        'cnpj' => Cnpj::class
    ];

    protected $fillable = [
        'nome',
        'cnpj',
        'numero_fidelidade'
    ];

    public function horario(): HasMany
    {
        return $this->hasMany(Horario::class);
    }
}
