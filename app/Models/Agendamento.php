<?php

namespace App\Models;

use App\Casts\DateTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Agendamento extends Model
{
    use HasFactory;

    protected $fillable = [
        'dia_hora_inicio',
        'dia_hora_fim',
        'user_id',
        'barbeiro_id',
        'status'
    ];

    protected $casts = [
        'dia_hora_inicio' => DateTime::class,
        'dia_hora_fim' => DateTime::class
    ];

    protected function diaHoraInicioCarbon(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => Carbon::make($attributes['dia_hora_inicio']),
        );
    }

    protected function diaHoraFimCarbon(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => Carbon::make($attributes['dia_hora_fim']),
        );
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function barbeiro(): BelongsTo
    {
        return $this->belongsTo(User::class, 'barbeiro_id', 'id');
    }

    public function agendamentoServico(): HasMany
    {
        return $this->hasMany(AgendamentoServico::class);
    }

    public function fluxoCaixa(): HasOne
    {
        return $this->hasOne(FluxoCaixa::class);
    }

    public function feedback(): HasOne
    {
        return $this->hasOne(Feedback::class);
    }

    public function fidelidade(): HasOne
    {
        return $this->hasOne(Fidelidade::class);
    }
}
