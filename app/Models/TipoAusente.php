<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TipoAusente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome',
    ];

    public function horarios(): BelongsTo
    {
        return $this->belongsTo(Ausente::class);
    }
}
