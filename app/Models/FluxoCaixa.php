<?php

namespace App\Models;

use App\Casts\Dinheiro;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FluxoCaixa extends Model
{
    use HasFactory;

    protected $dates = [
        'data'
    ];

    protected $casts = [
        'valor' => Dinheiro::class
    ];

    protected $fillable = [
        'descricao',
        'valor',
        'tipo_fluxo_id',
        'agendamento_id',
        'data'
    ];

    protected function valorFloat(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => $attributes['valor'],
        );
    }


    public function agendamento(): BelongsTo
    {
        return $this->belongsTo(Agendamento::class);
    }

    public function tipoFluxo(): BelongsTo
    {
        return $this->belongsTo(TipoFluxo::class);
    }
}
