<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Fidelidade extends Model
{
    use HasFactory;

    protected $dates = [
        'data_resgate'
    ];

    protected $fillable = [
        'data_resgate',
        'agendamento_id'
    ];

    public function agendamento(): BelongsTo
    {
        return $this->belongsTo(Agendamento::class);
    }
}
