<?php

namespace App\Services;

use App\Repositories\AgendamentoRepository;

class AgendamentoService
{
    public function __construct(
        AgendamentoRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    public function getAgendamento($id)
    {
        return $this->repository->getAgendamento($id);
    }

    public function update($id, $request) {
        return $this->repository->update($id, $request);
    }

    public function all() {
        return $this->repository->all();
    }

    public function getAllAgendados() {
        return $this->repository->getAllAgendados();
    }

    public function delete($id) {
        return $this->repository->delete($id);
    }

    public function agendamentoService($request) {
        return $this->repository->agendamentoService($request);
    }

    public function horario($request) {
        return $this->repository->horario($request);
    }

    public function concluido($id) {
        return $this->repository->concluido($id);
    }

    public function cancelado($id) {
        return $this->repository->cancelado($id);
    }

    public function getStatusAgendado() {
        return $this->repository->getStatusAgendado();
    }
}
