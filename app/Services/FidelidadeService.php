<?php

namespace App\Services;

use App\Repositories\FidelidadeRepository;

class FidelidadeService
{
    public function __construct(
        FidelidadeRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

}
