<?php

namespace App\Services;

use App\Repositories\ServicoRepository;

class ServicoService
{
    public function __construct(
        ServicoRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    public function getServico($id)
    {
        return $this->repository->getServico($id);
    }

    public function update($id, $request) {
        return $this->repository->update($id, $request);
    }

    public function all() {
        return $this->repository->all();
    }

    public function disable($id) {
        return $this->repository->disable($id);
    }
}
