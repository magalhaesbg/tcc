<?php

namespace App\Services;

use App\Repositories\TipoFluxoRepository;

class TipoFluxoService
{
    public function __construct(
        TipoFluxoRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function all() {
        return $this->repository->all();
    }
}
