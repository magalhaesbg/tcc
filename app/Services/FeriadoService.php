<?php

namespace App\Services;

use App\Repositories\FeriadoRepository;

class FeriadoService
{
    public function __construct(
        FeriadoRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function feriadosAposDiaAtual()
    {
        return $this->repository->feriadosAposDiaAtual();
    }

    public function getAllTipoFeriado()
    {
        return $this->repository->getAllTipoFeriado();
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    public function getFeriado($id)
    {
        return $this->repository->getFeriado($id);
    }

    public function update($id, $request)
    {
        return $this->repository->update($id, $request);
    }

}
