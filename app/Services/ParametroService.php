<?php

namespace App\Services;

use App\Repositories\ParametroRepository;

class ParametroService
{
    public function __construct(
        ParametroRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function update($id, $request)
    {
        return $this->repository->update($id, $request);
    }

}
