<?php

namespace App\Services;

use App\Helper\Util;
use App\Repositories\FluxoCaixaRepository;

class FluxoCaixaService
{
    public function __construct(
        FluxoCaixaRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    public function getFluxoCaixa($id)
    {
        return $this->repository->getFluxoCaixa($id);
    }

    public function update($id, $request) {
        return $this->repository->update($id, $request);
    }

    public function all() {
        return $this->repository->all();
    }

    public function delete($id) {
        return $this->repository->delete($id);
    }

    public function getDadosGrafico($date) {
        $arSemanas = Util::getWeekRange($date);

        $arDados = [];
        foreach($arSemanas AS $semana => $datas) {
            $arDados[$semana] = $this->repository->getFluxoCaixaPorRangeData($datas[0], $datas[1]);
        }

        $valorEntrada = 0.00;
        $valorSaida = 0.00;

        $arRetorno = [];
        $arRetorno[] = ['Semana', 'Entrada', 'Saída'];

        foreach($arDados AS $semana => $dados) {

            foreach($dados as $dado) {
                if ($dado->tipo_fluxo_id === 1) {
                    $valorEntrada += $dado->valorFloat;
                } else {
                    $valorSaida += $dado->valorFloat;
                }

                $arRetorno[$semana] = [$semana, $valorEntrada, $valorSaida];
            }
        }

        if (count($arRetorno) < 2) {
            $arRetorno[] = [0, 0, 0];
        }

        $arRetorno = array_values($arRetorno);

        return $arRetorno;
    }

    public function getDadosGraficoSemanal($date) {
        $arSemanas = Util::getWeekRange($date);

        $arDados = [];
        foreach($arSemanas AS $semana => $datas) {
            $arDados[$semana] = $this->repository->getFluxoCaixaPorRangeData($datas[0], $datas[1]);
        }

        $arRetorno = [];
        $arRetorno[] = ['Semana', 'Entrada', 'Saída'];
        foreach($arDados AS $semana => $dados) {
            $valorEntrada = 0.00;
            $valorSaida = 0.00;

            foreach($dados as $dado) {
                if ($dado->tipo_fluxo_id === 1) {
                    $valorEntrada += $dado->valorFloat;
                } else {
                    $valorSaida += $dado->valorFloat;
                }

                $arRetorno[$semana] = [$semana, $valorEntrada, $valorSaida];
            }
        }

        if (count($arRetorno) < 2) {
            $arRetorno[1] = [1, 0, 0];
        }

        $arRetorno = array_values($arRetorno);

        return $arRetorno;
    }
}
