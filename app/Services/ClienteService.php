<?php

namespace App\Services;

use App\Repositories\ClienteRepository;

class ClienteService
{
    public function __construct(
        ClienteRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    public function getCliente($id)
    {
        return $this->repository->getCliente($id);
    }

    public function update($id, $request) {
        return $this->repository->update($id, $request);
    }

    public function all() {
        return $this->repository->all();
    }

    public function disable($id) {
        return $this->repository->disable($id);
    }

    public function enable($id) {
        return $this->repository->enable($id);
    }

    public function updateSenha($request) {
        return $this->repository->updateSenha($request);
    }

    public function getClienteInformacoes($id) {
        return $this->repository->getClienteInformacoes($id);
    }

    public function fidelidade() {
        return $this->repository->fidelidade();
    }
}
