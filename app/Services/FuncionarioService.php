<?php

namespace App\Services;

use App\Helper\Util;
use App\Repositories\FuncionarioRepository;
use Carbon\Carbon;

class FuncionarioService
{
    public function __construct(
        FuncionarioRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    public function getFuncionario($id)
    {
        return $this->repository->getFuncionario($id);
    }

    public function update($id, $request) {
        return $this->repository->update($id, $request);
    }

    public function all() {
        return $this->repository->all();
    }

    public function disable($id) {
        return $this->repository->disable($id);
    }

    public function enable($id) {
        return $this->repository->enable($id);
    }

    public function updateSenha($request) {
        return $this->repository->updateSenha($request);
    }

    public function allBarbeiros() {
        return $this->repository->allBarbeiros();
    }

    public function getDadosGraficoBarbeiro($date) {
        $date = Carbon::make($date);
        $dateI = $date->copy()->startOfMonth();
        $dateF = $date->copy()->endOfMonth();

        $dados = $this->repository->getDadosGraficoBarbeiro($dateI->format('Y-m-d 00:00:00'), $dateF->format('Y-m-d 23:59:59'));

        $dados = $dados->groupBy('barbeiro_id');

        $arBarbeiro = [];
        $arBarbeiro[] = ['Barbeiro', 'Agendamentos Concluídos', (object) ['role' => "style"]];

        $indice = 0;
        foreach($dados AS $barbeiroId => $agendamentos) {
            $arBarbeiro[] = [ $agendamentos->first()->barbeiro->name, count($agendamentos), Util::rand_color($indice) ];
            $indice ++;
        }

        if (count($arBarbeiro) < 2) {
            $arBarbeiro[] = [0, 0, Util::rand_color(0)];
        }

        return $arBarbeiro;
    }

    public function getDadosGraficoBarbeiroAusente($date) {
        $date = Carbon::make($date);
        $dateI = $date->copy()->startOfMonth();
        $dateF = $date->copy()->endOfMonth();

        $dados = $this->repository->getDadosGraficoBarbeiroAusente($dateI->format('Y-m-d'), $dateF->format('Y-m-d'));

        $dados = $dados->groupBy('id');

        $arBarbeiro = [];
        $arBarbeiro[] = ['Barbeiro', 'Ausências', (object) ['role' => 'style']];

        $indice = 0;
        foreach($dados AS $barbeiroId => $user) {
            $arBarbeiro[] = [$user->first()->name, count($user->first()->ausente), Util::rand_color($indice)];
            $indice ++;
        }

        if (count($arBarbeiro) < 2) {
            $arBarbeiro[] = [0, 0, Util::rand_color(0)];
        }

        return $arBarbeiro;
    }
}
