<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService
{
    public function __construct(
        UserRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function getRolesFuncionario() {
        return $this->repository->getRolesFuncionario();
    }
}
