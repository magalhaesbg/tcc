<?php

namespace App\Services;

use App\Repositories\UserServicoRepository;

class UserServicoService
{
    public function __construct(
        UserServicoRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function getServicosBarbeiro($id)
    {
        return $this->repository->getServicosBarbeiro($id);
    }
}
