<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Arr;

class Util
{
    public static function validaCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string)$cnpj);

        if (strlen($cnpj) != 14)
            return false;

        if (preg_match('/(\d)\1{13}/', $cnpj))
            return false;

        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto))
            return false;

        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);

    }

    public static function validaCPF($cpf)
    {
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        if (strlen($cpf) != 11) {
            return false;
        }

        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }

        return true;
    }

    public static function getWeekRange($date){
        $f = 'Y-m-d';

        $today = Carbon::make($date);
        $date  = $today->copy()->firstOfMonth()->startOfDay();
        $eom   = $today->copy()->endOfMonth()->startOfDay();

        $dates = [];

        for($i = 1; $date->lte($eom); $i++){
            $startDate = $date->copy();

            while($date->dayOfWeek != Carbon::SATURDAY && $date->lt($eom)){
                $date->addDay();
            }

            $dates[$i] = [$startDate->format($f), $date->format($f)];
            $date->addDay();
          }

        return $dates;
    }

    public static function rand_color($indice) {
        $colors = [
            '#A9A9A9',
            '#87CEFA',
            '#4682B4',
            '#00CED1',
            '#008B8B',
            '#5F9EA0',
            '#00FA9A',
            '#00FF7F',
            '#90EE90',
            '#2E8B57',
            '#32CD32',
            '#9ACD32',
            '#6B8E23',
            '#BDB76B',
            '#B8860B',
            '#A0522D',
            '#F4A460',
            '#DEB887',
            '#7B68EE',
            '#EE82EE',
        ];

        return Arr::get($colors, $indice, '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT));
    }
}
