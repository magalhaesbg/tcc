<?php

namespace App\Http\Controllers;

use App\Services\FeriadoService;
use App\Services\ParametroService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ConfiguracaoController extends Controller
{
    public function __construct(
        ParametroService $parametroService,
        FeriadoService   $feriadoService,
    ) {
        $this->middleware('auth');

        $this->parametroService = $parametroService;
        $this->feriadoService   = $feriadoService;
    }

    public function parametro() {
        $parametros = $this->parametroService->getAll();

        return View('telas.configuracao.parametro.index', [
            'parametros' => $parametros
        ]);
    }

    public function update($id) {
        $parametros = $this->parametroService->update( $id, request() );

        return redirect()->route('configuracao.parametro')->with('mensagem', $parametros);
    }

    public function feriado(Request $request) {

        if ($request->ajax()) {
            $data = $this->feriadoService->feriadosAposDiaAtual();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('dia', function($r) {
                        return '<span style="display:none">' . $r->inicio->format('Ymd') . '</span>' . $r->inicio->format('d/m/Y');
                    })
                    ->addColumn('action', function($row){
                        $btn  = "<a href='".route('configuracao.feriado.edit', $row->id)."' class='btn btn-warning btn-sm' style='margin-right:.5rem'>Editar</a>";
                        $btn .= "<a href='".route('configuracao.feriado.delete', $row->id)."' class='btn btn-danger btn-sm'>Deletar</a>";
                        return $btn;
                    })
                    ->rawColumns(['action', 'dia'])
                    ->make(true);
        }

        return View('telas.configuracao.feriado.index');
    }

    public function create() {
        $tipoFeriado = $this->feriadoService->getAllTipoFeriado();

        return View('telas.configuracao.feriado.create', [
            'tipoFeriado' => $tipoFeriado
        ]);
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'nome'              => ['required', 'string'],
            'inicio'            => ['required', 'date'],
            'tipo_feriado_id'   => ['required', 'exists:tipo_feriados,id'],
        ], [
            'nome.required'             => 'O campo Nome é obrigatório',
            'inicio.required'           => 'O campo Início é obrigatório',
            'tipo_feriado_id.required'  => 'O campo Tipo Feriado é obrigatório.',
            'tipo_feriado_id.exists'    => 'Campo Tipo Feriado selecionado é inválido.',
        ]);

        if ($this->feriadoService->store($request)) {
            return redirect()->route('configuracao.feriado')->with(['mensagem' => 'Feriado inserido com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('configuracao.feriado')->with(['mensagem' => 'Erro ao tentar adicionar feriado.', 'tipo' => 'error']);
        }
    }

    public function delete($id) {
        if ($this->feriadoService->delete($id)) {
            return redirect()->route('configuracao.feriado')->with(['mensagem' => 'Feriado deletado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('configuracao.feriado')->with(['mensagem' => 'Erro ao tentar deletar o feriado.', 'tipo' => 'error']);
        }
    }

    public function edit($id) {
        $tipoFeriado = $this->feriadoService->getAllTipoFeriado();
        $feriado = $this->feriadoService->getFeriado($id);

        return View('telas.configuracao.feriado.edit', [
            'feriado'       => $feriado,
            'tipoFeriado'   => $tipoFeriado
        ]);
    }

    public function updateFeriado($id, Request $request) {
        $validated = $request->validate([
            'nome'              => ['required', 'string'],
            'inicio'            => ['required', 'date'],
            'tipo_feriado_id'   => ['required', 'exists:tipo_feriados,id'],
        ], [
            'nome.required'             => 'O campo Nome é obrigatório',
            'inicio.required'           => 'O campo Início é obrigatório',
            'tipo_feriado_id.required'  => 'O campo Tipo Feriado é obrigatório.',
            'tipo_feriado_id.exists'    => 'Campo Tipo Feriado selecionado é inválido.',
        ]);

        if ($this->feriadoService->update($id, $request)) {
            return redirect()->route('configuracao.feriado')->with(['mensagem' => 'Feriado atualizado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('configuracao.feriado')->with(['mensagem' => 'Erro ao tentar atualizar feriado.', 'tipo' => 'error']);
        }
    }
}
