<?php

namespace App\Http\Controllers;

use App\Rules\Cpf;
use App\Services\ClienteService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class ClienteController extends Controller
{
    public function __construct(
        ClienteService     $clienteService,
        UserService        $userService
    ) {
        $this->middleware('auth');
        $this->clienteService   = $clienteService;
        $this->userService          = $userService;
    }

    public function index(Request $request) {

        if ($request->ajax()) {
            $data = $this->clienteService->all();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn   = "<a href='".route('cliente.show', $row->id)."' class='btn btn-primary btn-sm' style='margin-right:.5rem'>Visualizar</a>";
                        $btn  .= "<a href='".route('cliente.edit', $row->id)."' class='btn btn-warning btn-sm'>Editar</a>";
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return View('telas.cliente.index');
    }

    public function create() {
        return View('telas.cliente.create');
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'name'          => ['required', 'string'],
            'cpf'           => ['required', new Cpf()],
            'email'         => ['required', 'email'],
        ], [
            'name.required'    => 'O campo Nome é obrigatório',
            'cpf.required'     => 'O campo CPF é obrigatório',
            'email.required'   => 'O campo Email é obrigatório',
        ]);

        $request->request->add(['password' => Hash::make(str_replace(['.', '-'], ['', ''], $request->cpf))]);

        if ($this->clienteService->store($request)) {
            return redirect()->route('cliente.index')->with(['mensagem' => 'Cliente inserido com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('cliente.index')->with(['mensagem' => 'Erro ao tentar adicionar cliente.', 'tipo' => 'error']);
        }
    }

    public function edit($id) {
        $cliente = $this->clienteService->getCliente($id);

        return View('telas.cliente.edit', [
            'cliente'   => $cliente,
        ]);
    }

    public function update($id, Request $request) {
        $validated = $request->validate([
            'name'          => ['required', 'string'],
            'cpf'           => ['required', new Cpf()],
            'email'         => ['required', 'email'],
        ], [
            'name.required'    => 'O campo Nome é obrigatório',
            'cpf.required'     => 'O campo CPF é obrigatório',
            'email.required'   => 'O campo Email é obrigatório',
            'role.required'    => 'O campo Função é obrigatório'
        ]);

        if ($this->clienteService->update($id, $request)) {
            return redirect()->route('cliente.index')->with(['mensagem' => 'Cliente alterado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('cliente.index')->with(['mensagem' => 'Erro ao tentar alterar o cliente.', 'tipo' => 'error']);
        }
    }

    public function show($id) {
        $cliente = $this->clienteService->getClienteInformacoes($id);

        return View('telas.cliente.show', [
            'cliente' => $cliente
        ]);
    }
}
