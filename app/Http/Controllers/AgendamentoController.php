<?php

namespace App\Http\Controllers;

use App\Services\AgendamentoService;
use App\Services\ClienteService;
use App\Services\FuncionarioService;
use App\Services\ServicoService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AgendamentoController extends Controller
{
    public function __construct(
        AgendamentoService   $agendamentoService,
        FuncionarioService   $funcionarioService,
        ClienteService       $clienteService,
        ServicoService       $servicoService,
    ) {
        $this->middleware('auth');
        $this->agendamentoService    = $agendamentoService;
        $this->funcionarioService    = $funcionarioService;
        $this->clienteService        = $clienteService;
        $this->servicoService        = $servicoService;
    }

    public function index(Request $request) {

        if ($request->ajax()) {
            $data = $this->agendamentoService->all();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('status', function($r){
                        switch($r->status) {
                            case 'Concluído':
                                return '<span class="badge bg-success">'.$r->status.'</span>';
                                break;
                            case 'Agendado':
                                return '<span class="badge bg-primary">'.$r->status.'</span>';
                                break;
                            case 'Cancelado':
                                return '<span class="badge bg-danger">'.$r->status.'</span>';
                                break;
                        }
                    })
                    ->editColumn('valor', function($r){
                        $valor = 0.00;
                        foreach($r->agendamentoServico AS $servico) {
                            $valor += str_replace(['.', ','], ['', '.'], $servico->valor);
                        }
                        return 'R$ '.$valor;
                    })
                    ->addColumn('action', function($row){
                        $btn  = "<a href='".route('agendamento.show', $row->id)."' class='btn btn-primary btn-sm' style='margin-right:.5rem'>Visualizar</a>";
                        $btn .= "<a href='".route('agendamento.edit', $row->id)."' class='btn btn-warning btn-sm' style='margin-right:.5rem'>Editar</a>";
                        $btn .= "<button id='desativar' data-url='".route('agendamento.delete', $row->id)."' type='button' data-bs-toggle='modal' data-bs-target='#alert_modal' class='btn btn-danger btn-sm'>Deletar</button>";
                        return $btn;
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
        }

        return View('telas.agendamento.index');
    }

    public function create() {
        $barbeiros = $this->funcionarioService->allBarbeiros();
        $clientes = $this->clienteService->all();
        $servicos = $this->servicoService->all();

        return View('telas.agendamento.create', [
            'barbeiros' => $barbeiros,
            'clientes'  => $clientes,
            'servicos'  => $servicos
        ]);
    }

    public function show($id) {
        $agendamento = $this->agendamentoService->getAgendamento($id);
        $servicos = $this->servicoService->all();

        return View('telas.agendamento.show', [
            'agendamento' => $agendamento,
            'servicos'    => $servicos
        ]);
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'user_id'        => ['sometimes', 'required', 'exists:users,id'],
            'barbeiro'       => ['required', 'exists:users,id'],
            'servicos'       => ['required', 'array'],
            'tempo_estimado' => ['required', 'integer'],
            'dia'            => ['required', 'date'],
            'hora'           => ['required']
        ], [
            'user_id.required'          => 'O campo Cliente é obrigatório',
            'barbeiro.required'         => 'O campo Barbeiro é obrigatório',
            'servicos.required'         => 'Ao menos um serviço tem de ser selecionado.',
            'tempo_estimado.required'   => 'O campo Tempo Estimado é obrigatório.',
            'dia.required'              => 'O campo Dia é obrigatório',
            'hora.required'             => 'O campo Horário é obrigatório'
        ]);

        if ($this->agendamentoService->store($request)) {
            return redirect()->route('agendamento.index')->with(['mensagem' => 'Agendamento inserido com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('agendamento.index')->with(['mensagem' => 'Erro ao tentar adicionar agendamento.', 'tipo' => 'error']);
        }
    }

    public function edit($id) {
        $agendamento = $this->agendamentoService->getAgendamento($id);
        $barbeiros = $this->funcionarioService->allBarbeiros();
        $clientes = $this->clienteService->all();
        $servicos = $this->servicoService->all();

        return View('telas.agendamento.edit', [
            'barbeiros'   => $barbeiros,
            'clientes'    => $clientes,
            'servicos'    => $servicos,
            'agendamento' => $agendamento
        ]);
    }

    public function update($id, Request $request) {
        $validated = $request->validate([
            'barbeiro'       => ['required', 'exists:users,id'],
            'servicos'       => ['required', 'array'],
            'tempo_estimado' => ['required', 'integer'],
            'dia'            => ['required', 'date'],
            'hora'           => ['required']
        ], [
            'barbeiro.required'         => 'O campo Barbeiro é obrigatório',
            'servicos.required'         => 'Ao menos um serviço tem de ser selecionado.',
            'tempo_estimado.required'   => 'O campo Tempo Estimado é obrigatório.',
            'dia.required'              => 'O campo Dia é obrigatório',
            'hora.required'             => 'O campo Horário é obrigatório'
        ]);

        if ($this->agendamentoService->update($id, $request)) {
            return redirect()->route('agendamento.index')->with(['mensagem' => 'Agendamento alterado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('agendamento.index')->with(['mensagem' => 'Erro ao tentar alterar o agendamento.', 'tipo' => 'error']);
        }
    }

    public function delete($id) {
        if ($this->agendamentoService->delete($id)) {
            return redirect()->route('agendamento.index')->with(['mensagem' => 'Agendamento deletado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('agendamento.index')->with(['mensagem' => 'Erro ao tentar deletar o agendamento.', 'tipo' => 'error']);
        }
    }

    public function horario(Request $request) {
        $horarios = $this->agendamentoService->horario($request);

        return json_encode($horarios);
    }

    public function concluido($id) {
        if ($this->agendamentoService->concluido($id)) {
            return redirect()->route('dashboard')->with(['mensagem' => 'Agendamento concluído com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('dashboard')->with(['mensagem' => 'Erro ao tentar concluir o agendamento.', 'tipo' => 'error']);
        }
    }

    public function cancelado($id) {
        if ($this->agendamentoService->cancelado($id)) {
            return redirect()->route('dashboard')->with(['mensagem' => 'Agendamento cancelado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('dashboard')->with(['mensagem' => 'Erro ao tentar cancelar o agendamento.', 'tipo' => 'error']);
        }
    }
}
