<?php

namespace App\Http\Controllers;

use App\Models\TipoFluxo;
use App\Rules\Cpf;
use App\Services\FluxoCaixaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class FluxoCaixaController extends Controller
{
    public function __construct(
        FluxoCaixaService   $fluxocaixaService,
        TipoFluxo           $tipoFluxo,
    ) {
        $this->middleware('auth');
        $this->fluxocaixaService    = $fluxocaixaService;
        $this->tipoFluxo            = $tipoFluxo;
    }

    public function index(Request $request) {

        if ($request->ajax()) {
            $data = $this->fluxocaixaService->all();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('valor', function($r){
                        return 'R$ '.$r->valor;
                    })
                    ->editColumn('data', function($r){
                        return '<span style="display:none">' . $r->data->format('Ymd') . '</span>' . $r->data->format('d/m/Y');
                    })
                    ->editColumn('tipo_fluxo.nome', function($r){
                        if ($r->tipo_fluxo_id == 1) {
                            return view('components.bagde', ['codigoBadge' => 'success', 'mensagemBadge' => $r->tipoFluxo->nome]);
                        }
                        return view('components.bagde', ['codigoBadge' => 'danger', 'mensagemBadge' => $r->tipoFluxo->nome]);
                    })
                    ->addColumn('action', function($row){
                        $btn  = "<a href='".route('fluxocaixa.edit', $row->id)."' class='btn btn-warning btn-sm' style='margin-right:.5rem'>Editar</a>";
                        $btn .= "<button id='desativar' data-url='".route('fluxocaixa.delete', $row->id)."' type='button' data-bs-toggle='modal' data-bs-target='#alert_modal' class='btn btn-danger btn-sm'>Deletar</button>";
                        return $btn;
                    })
                    ->rawColumns(['action', 'data'])
                    ->make(true);
        }

        return View('telas.fluxocaixa.index');
    }

    public function create() {
        $tipoFluxo = $this->tipoFluxo->all();

        return View('telas.fluxocaixa.create', [
            'tipoFluxo' => $tipoFluxo
        ]);
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'data'          => ['required', 'date'],
            'valor'         => ['required'],
            'tipo_fluxo_id' => ['required', 'exists:tipo_fluxos,id'],
        ], [
            'data.required'             => 'O campo Data é obrigatório',
            'valor.required'            => 'O campo Valor é obrigatório',
            'tipo_fluxo_id.required'    => 'O campo Tipo de Movimentação é obrigatório',
            'tipo_fluxo_id.exists'      => 'O campo Tipo de Movimentação possui um valor inválido.'
        ]);

        if ($this->fluxocaixaService->store($request)) {
            return redirect()->route('fluxocaixa.index')->with(['mensagem' => 'Movimentação inserida com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('fluxocaixa.index')->with(['mensagem' => 'Erro ao tentar adicionar movimentação.', 'tipo' => 'error']);
        }
    }

    public function edit($id) {
        $fluxoCaixa = $this->fluxocaixaService->getFluxoCaixa($id);
        $tipoFluxo = $this->tipoFluxo->all();

        return View('telas.fluxocaixa.edit', [
            'fluxoCaixa'   => $fluxoCaixa,
            'tipoFluxo'    => $tipoFluxo
        ]);
    }

    public function update($id, Request $request) {
        $validated = $request->validate([
            'data'          => ['required', 'date'],
            'valor'         => ['required'],
            'tipo_fluxo_id' => ['required', 'exists:tipo_fluxos,id'],
        ], [
            'data.required'             => 'O campo Data é obrigatório',
            'valor.required'            => 'O campo Valor é obrigatório',
            'tipo_fluxo_id.required'    => 'O campo Tipo de Movimentação é obrigatório',
            'tipo_fluxo_id.exists'      => 'O campo Tipo de Movimentação possui um valor inválido.'
        ]);

        if ($this->fluxocaixaService->update($id, $request)) {
            return redirect()->route('fluxocaixa.index')->with(['mensagem' => 'Movimentação alterada com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('fluxocaixa.index')->with(['mensagem' => 'Erro ao tentar alterar a movimentação.', 'tipo' => 'error']);
        }
    }

    public function delete($id) {
        if ($this->fluxocaixaService->delete($id)) {
            return redirect()->route('fluxocaixa.index')->with(['mensagem' => 'Movimentação deletada com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('fluxocaixa.index')->with(['mensagem' => 'Erro ao tentar deletar a movimentação.', 'tipo' => 'error']);
        }
    }
}
