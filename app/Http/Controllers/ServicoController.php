<?php

namespace App\Http\Controllers;

use App\Services\ServicoService;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ServicoController extends Controller
{
    public function __construct(
        ServicoService $servicoService
    ) {
        $this->middleware('auth');
        $this->servicoService = $servicoService;
    }

    public function index(Request $request) {

        if ($request->ajax()) {
            $data = $this->servicoService->all();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $btn  = "<a href='".route('servico.edit', $row->id)."' class='btn btn-warning btn-sm' style='margin-right:.5rem'>Editar</a>";
                            $btn .= "<button id='desativar' data-url='".route('servico.disable', $row->id)."' type='button' data-bs-toggle='modal' data-bs-target='#alert_modal' class='btn btn-danger btn-sm'>Desativar</button>";
                            return $btn;
                    })
                    ->editColumn('tempo_estimado', function ($r){
                        return $r->tempo_estimado.' min';
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return View('telas.servico.index');
    }

    public function create() {
        return View('telas.servico.create');
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'nome'           => ['required', 'string'],
            'valor'          => ['required'],
            'tempo_estimado' => ['required', 'integer']
        ], [
            'nome.required'             => 'O campo Nome é obrigatório',
            'valor.required'            => 'O campo Valor é obrigatório',
            'tempo_estimado.required'   => 'O campo Tempo Estimado é obrigatório',
            'tempo_estimado.integer'    => 'O campo Tempo Estimado deve ser um número inteiro'
        ]);

        if ($this->servicoService->store($request)) {
            return redirect()->route('servico.index')->with(['mensagem' => 'Serviço inserido com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('servico.index')->with(['mensagem' => 'Erro ao tentar adicionar serviço.', 'tipo' => 'error']);
        }
    }

    public function edit($id) {
        $servico = $this->servicoService->getServico($id);

        return View('telas.servico.edit', [
            'servico' => $servico
        ]);
    }

    public function update($id, Request $request) {
        $validated = $request->validate([
            'nome'           => ['required', 'string'],
            'valor'          => ['required'],
            'tempo_estimado' => ['required', 'integer']
        ], [
            'nome.required'             => 'O campo Nome é obrigatório',
            'valor.required'            => 'O campo Valor é obrigatório',
            'tempo_estimado.required'   => 'O campo Tempo Estimado é obrigatório',
            'tempo_estimado.integer'    => 'O campo Tempo Estimado deve ser um número inteiro'
        ]);

        if ($this->servicoService->update($id, $request)) {
            return redirect()->route('servico.index')->with(['mensagem' => 'Serviço alterado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('servico.index')->with(['mensagem' => 'Erro ao tentar alterar o serviço.', 'tipo' => 'error']);
        }
    }

    public function disable($id) {
        if ($this->servicoService->disable($id)) {
            return redirect()->route('servico.index')->with(['mensagem' => 'Serviço desativado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('servico.index')->with(['mensagem' => 'Erro ao tentar desativar o serviço.', 'tipo' => 'error']);
        }
    }
}
