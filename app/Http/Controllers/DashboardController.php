<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Parametro;
use App\Services\AgendamentoService;
use App\Services\ClienteService;
use App\Services\FluxoCaixaService;
use App\Services\FuncionarioService;
use App\Services\ServicoService;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct(
        ServicoService      $servicoService,
        AgendamentoService  $agendamentoService,
        ClienteService      $clienteService,
        FluxoCaixaService   $fluxoCaixaService,
        FuncionarioService  $funcionarioService,
    ) {
        $this->middleware('auth');
        $this->servicoService       = $servicoService;
        $this->agendamentoService   = $agendamentoService;
        $this->clienteService       = $clienteService;
        $this->fluxoCaixaService    = $fluxoCaixaService;
        $this->funcionarioService   = $funcionarioService;
    }

    public function index()
    {
        if (Auth::user()->hasRole(['Gerente'])) {
            if (isset(request()->mes) && isset(request()->ano)) {
                $date = request()->ano . '-' . request()->mes . '-' . '01';
            } else {
                $date = date('Y-m-d');
            }
            $dadosFluxoCaixa        = $this->fluxoCaixaService->getDadosGrafico( $date );
            $dadosFluxoCaixaSemanal = $this->fluxoCaixaService->getDadosGraficoSemanal( $date );
            $dadosBarbeiros         = $this->funcionarioService->getDadosGraficoBarbeiro( $date );
            $dadosBarbeiroAusente   = $this->funcionarioService->getDadosGraficoBarbeiroAusente( $date );

            return View('telas.dashboard.index', [
                'dadosFluxoCaixa'        => $dadosFluxoCaixa,
                'dadosFluxoCaixaSemanal' => $dadosFluxoCaixaSemanal,
                'dadosBarbeiros'         => $dadosBarbeiros,
                'dadosBarbeiroAusente'   => $dadosBarbeiroAusente,
                'mes'                    => request()->mes,
                'ano'                    => request()->ano
            ]);

        } elseif(Auth::user()->hasRole(['Barbeiro'])) {
            $agendamentos = $this->agendamentoService->getAllAgendados();
            $servico = $this->servicoService->all();

            return View('telas.dashboard.index', [
                'servicos'      => $servico,
                'agendamentos'  => $agendamentos
            ]);

        } elseif(Auth::user()->hasRole(['Atendente'])) {
            $agendamentos = $this->agendamentoService->getAllAgendados();
            $servico = $this->servicoService->all();

            return View('telas.dashboard.index', [
                'servicos'      => $servico,
                'agendamentos'  => $agendamentos
            ]);

        } elseif(Auth::user()->hasRole(['Cliente'])) {
            $agendamentos       = $this->agendamentoService->getStatusAgendado();
            $servico            = $this->servicoService->all();
            $fidelidade         = $this->clienteService->fidelidade();
            $configFidelidade   = Parametro::all()->first()->numero_fidelidade;

            return View('telas.dashboard.index', [
                'servicos'          => $servico,
                'agendamentos'      => $agendamentos,
                'fidelidade'        => $fidelidade,
                'configFidelidade'  => $configFidelidade
            ]);

        }
    }
}
