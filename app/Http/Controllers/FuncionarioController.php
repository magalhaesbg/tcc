<?php

namespace App\Http\Controllers;

use App\Rules\Cpf;
use App\Services\FuncionarioService;
use App\Services\ServicoService;
use App\Services\UserService;
use App\Services\UserServicoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class FuncionarioController extends Controller
{
    public function __construct(
        FuncionarioService $funcionarioService,
        UserService        $userService,
        ServicoService     $servicoService,
        UserServicoService $userServicoService,
    ) {
        $this->middleware('auth');
        $this->funcionarioService   = $funcionarioService;
        $this->userService          = $userService;
        $this->servicoService       = $servicoService;
        $this->userServicoService   = $userServicoService;
    }

    public function index(Request $request) {

        if ($request->ajax()) {
            $data = $this->funcionarioService->all();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                            $btn  = "<a href='".route('funcionario.edit', $row->id)."' class='btn btn-warning btn-sm' style='margin-right:.5rem'>Editar</a>";
                            if ($row->ativo === 0) {
                                $btn .= "<a href='".route('funcionario.enable', $row->id)."' class='btn btn-sm btn-success'>Ativar</a>";
                            } else {
                                $btn .= "<button id='desativar' data-url='".route('funcionario.disable', $row->id)."' type='button' data-bs-toggle='modal' data-bs-target='#alert_modal' class='btn btn-danger btn-sm'>Desativar</button>";
                            }
                            return $btn;
                    })
                    ->editColumn('ativo', function ($r){
                        if ($r->ativo === 0) {
                            return view('components.bagde', ['codigoBadge' => 'secondary', 'mensagemBadge' => 'Desativado']);
                        } else {
                            return view('components.bagde', ['codigoBadge' => 'primary', 'mensagemBadge' => 'Ativado']);
                        }
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return View('telas.funcionario.index');
    }

    public function create() {
        $funcoes  = $this->userService->getRolesFuncionario();
        $servicos = $this->servicoService->all();

        return View('telas.funcionario.create', [
            'funcoes'  => $funcoes,
            'servicos' => $servicos
        ]);
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'name'          => ['required', 'string'],
            'cpf'           => ['required', new Cpf()],
            'email'         => ['required', 'email'],
            'role'          => ['required', 'exists:roles,id'],
            'servicos'      => $request->role == 2 ? ['required', 'array'] : ''
        ], [
            'name.required'    => 'O campo Nome é obrigatório',
            'cpf.required'     => 'O campo CPF é obrigatório',
            'email.required'   => 'O campo Email é obrigatório',
            'role.required'    => 'O campo Função é obrigatório',
            'servicos.required'=> 'É obrigatório selecionar um serviço.'
        ]);

        $request->request->add(['password' => Hash::make(str_replace(['.', '-'], ['', ''], $request->cpf))]);

        if ($this->funcionarioService->store($request)) {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Funcionário inserido com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Erro ao tentar adicionar funcionário.', 'tipo' => 'error']);
        }
    }

    public function edit($id) {
        $funcoes = $this->userService->getRolesFuncionario();
        $funcionario = $this->funcionarioService->getFuncionario($id);
        $servicos = $this->servicoService->all();

        return View('telas.funcionario.edit', [
            'funcionario'   => $funcionario,
            'funcoes'       => $funcoes,
            'servicos'      => $servicos
        ]);
    }

    public function update($id, Request $request) {
        $validated = $request->validate([
            'name'          => ['required', 'string'],
            'cpf'           => ['required', new Cpf()],
            'email'         => ['required', 'email'],
            'role'          => ['required', 'exists:roles,id'],
            'servicos'      => $request->role == 2 ? ['required', 'array'] : ''
        ], [
            'name.required'    => 'O campo Nome é obrigatório',
            'cpf.required'     => 'O campo CPF é obrigatório',
            'email.required'   => 'O campo Email é obrigatório',
            'role.required'    => 'O campo Função é obrigatório',
            'servicos.required'=> 'É obrigatório selecionar um serviço.'
        ]);

        if ($this->funcionarioService->update($id, $request)) {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Funcionário alterado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Erro ao tentar alterar o funcionário.', 'tipo' => 'error']);
        }
    }

    public function disable($id) {
        $disabled = $this->funcionarioService->disable($id);
        if ($disabled['status'] == 'success') {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Funcionário desativado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Erro ao tentar desativar o funcionário. '.$disabled['message'], 'tipo' => 'error']);
        }
    }

    public function enable($id) {
        if ($this->funcionarioService->enable($id)) {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Funcionário ativado com sucesso.', 'tipo' => 'success']);
        } else {
            return redirect()->route('funcionario.index')->with(['mensagem' => 'Erro ao tentar ativar o funcionário.', 'tipo' => 'error']);
        }
    }

    public function senha() {
        return View('auth.senha');
    }

    public function updateSenha(Request $request) {
        $validated = $request->validate([
            'password'              => ['required', 'confirmed'],
            'password_confirmation' => ['required'],
        ], [
            'password.required'                 => 'O campo Nome é obrigatório',
            'password.confirmed'                => 'O campo Senha e Confirmar senha devem ter o mesmo valor',
            'password_confirmation.required'    => 'O campo Confirmar senha é obrigatório'
        ]);

        if($this->funcionarioService->updateSenha($request)) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->back();
        }
    }

    public function getServicosBarbeiro(Request $request) {
        $servicos = $this->userServicoService->getServicosBarbeiro($request->barbeiro_id);

        return json_encode($servicos);
    }
}
