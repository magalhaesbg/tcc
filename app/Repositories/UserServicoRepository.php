<?php

namespace App\Repositories;

use App\Models\UserServico;

class UserServicoRepository
{
    public function getServicosBarbeiro($id) {
        return UserServico::where('user_id', $id)->get();
    }
}
