<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role;

class UserRepository
{
    public function getRolesFuncionario() {
        return Role::whereIn('name', ['Barbeiro', 'Atendente'])->orderBy('name')->get();
    }
}
