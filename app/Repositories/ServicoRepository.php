<?php

namespace App\Repositories;

use App\Models\Servico;
use Exception;

class ServicoRepository
{
    public function store($request) {
        try {
            return Servico::create($request->all());
        } catch(Exception $e) {
            return false;
        }
    }

    public function getServico($id)
    {
        $servico = Servico::find($id);

        return $servico;
    }

    public function update($id, $request) {
        try {
            return Servico::find($id)->update($request->all());
        } catch(Exception $e) {
            return false;
        }
    }

    public function all() {
        return Servico::where('ativo', 1)->orderBy('nome')->get();
    }

    public function disable($id) {
        try {
            return Servico::find($id)->update(['ativo' => 0]);
        } catch(Exception $e) {
            return false;
        }
    }
}
