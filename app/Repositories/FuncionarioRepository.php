<?php

namespace App\Repositories;

use App\Models\Agendamento;
use App\Models\User;
use App\Models\UserServico;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class FuncionarioRepository
{
    public function store($request) {
        try {
            $role = Role::find($request->role);
            $user = User::create($request->all());

            $user->assignRole($role->name);

            UserServico::where('user_id', $user->id)->delete();

            foreach($request->servicos AS $id => $boolean) {
                UserServico::create(['user_id' => $user->id, 'servico_id' => $id]);
            }

            return $user;
        } catch(Exception $e) {
            return false;
        }
    }

    public function getFuncionario($id)
    {
        $funcionario = User::with(['roles'])->find($id);

        return $funcionario;
    }

    public function update($id, $request) {
        try {
            $newRole = Role::find($request->role);
            User::find($id)->update($request->all());
            $user = User::with(['roles'])->where('id', $id)->get()->first();

            foreach($user->roles AS $role) {
                $user->removeRole($role->name);
            }

            $user->assignRole($newRole->name);

            UserServico::where('user_id', $user->id)->delete();

            foreach($request->servicos AS $id => $boolean) {
                UserServico::create(['user_id' => $user->id, 'servico_id' => $id]);
            }

            return $user;
        } catch(Exception $e) {
            return false;
        }
    }

    public function all() {
        return User::role(['Barbeiro', 'Atendente'])->with(['roles'])->get();
    }

    public function disable($id) {
        try {
            $agendamento = Agendamento::where('barbeiro_id', $id)->where('dia_hora_inicio', '>=', date('Y-m-d'))->where('finalizado', 0)->get();

            if (count($agendamento) > 0) {
                return ['status' => 'erro', 'message' => 'Funcionário tem agendamentos futuros pendentes.'];
            }

            return ['status' => 'success', 'message' => User::find($id)->update(['ativo' => 0])];
        } catch(Exception $e) {
            return ['status' => 'erro', 'message' => ''];
        }
    }

    public function enable($id) {
        try {
            return User::find($id)->update(['ativo' => 1]);
        } catch(Exception $e) {
            return false;
        }
    }

    public function updateSenha($request) {
        try {
            $senhaHash = Hash::make($request->password);

            $user = User::find( Auth::user()->id )->update(['password' => $senhaHash, 'changed_password' => date('Y-m-d H:i:s')]);

            return $user;
        } catch(Exception $e) {
            return false;
        }
    }

    public function allBarbeiros() {
        return User::role(['Barbeiro'])->with(['roles'])->where('ativo', 1)->orderBy('name')->get();
    }

    public function getDadosGraficoBarbeiro($dateI, $dateF) {
        return Agendamento::with(['barbeiro'])
            ->where('dia_hora_inicio', '>=', $dateI)
            ->where('dia_hora_fim', '<=', $dateF)
            ->where('status', 'Concluído')
            ->get();
    }

    public function getDadosGraficoBarbeiroAusente($dateI, $dateF) {
        return User::with('ausente')->whereHas('ausente', function($q) use($dateI, $dateF) {
            $q->where('dia', '>=', $dateI)->where('dia', '<=', $dateF);
        })->get();
    }
}
