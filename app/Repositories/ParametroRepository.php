<?php

namespace App\Repositories;

use App\Models\Horario;
use App\Models\Parametro;
use Exception;

class ParametroRepository
{
    public function getAll() {
        return Parametro::with('horario')->get()->first();
    }

    public function update($id, $request) {
        try {
            $dadosParametro = [
                'nome'              => $request->nome,
                'cnpj'              => $request->cnpj,
                'numero_fidelidade' => $request->numero_fidelidade,
            ];
            Parametro::find($id)->update($dadosParametro);

            Horario::where('parametro_id', $id)->delete();

            foreach($request->horario AS $diaId => $horarios) {
                if (!is_null($horarios['entrada_manha']) || !is_null($horarios['saida_manha']) || !is_null($horarios['entrada_tarde']) || !is_null($horarios['saida_tarde'])) {
                    Horario::create([
                        'parametro_id'  => 1,
                        'dia_semana_id' => $diaId,
                        'entrada_manha' => $horarios['entrada_manha'],
                        'saida_manha'   => $horarios['saida_manha'],
                        'entrada_tarde' => $horarios['entrada_tarde'],
                        'saida_tarde'   => $horarios['saida_tarde']
                    ]);
                }
            }

            return Parametro::find($id);
        } catch(Exception $e) {
            return false;
        }
    }
}
