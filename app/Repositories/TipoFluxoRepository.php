<?php

namespace App\Repositories;

use App\Models\TipoFluxo;

class TipoFluxoRepository
{
    public function all() {
        return TipoFluxo::all();
    }
}
