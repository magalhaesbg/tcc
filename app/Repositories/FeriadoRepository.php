<?php

namespace App\Repositories;

use App\Models\Feriado;
use App\Models\TipoFeriado;
use Exception;

class FeriadoRepository
{
    public function feriadosAposDiaAtual() {
        return Feriado::with(['tipoFeriado'])->where('inicio', '>=', date('Y-m-d') . ' 00:00:00')->orderBy('inicio')->get();
    }

    public function getAllTipoFeriado() {
        return TipoFeriado::all();
    }

    public function store($request) {
        try {
            Feriado::create([
                'nome'              => $request->nome,
                'inicio'            => $request->inicio . ' 00:00:00',
                'fim'               => $request->inicio . ' 23:59:59',
                'tipo_feriado_id'   => $request->tipo_feriado_id
            ]);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete($id) {
        try {
            Feriado::where('id', $id)->delete();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getFeriado($id) {
        return Feriado::find($id);
    }

    public function update($id, $request) {
        try {
            Feriado::where('id', $id)->update([
                'nome'              => $request->nome,
                'inicio'            => $request->inicio . ' 00:00:00',
                'fim'               => $request->inicio . ' 23:59:59',
                'tipo_feriado_id'   => $request->tipo_feriado_id
            ]);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
