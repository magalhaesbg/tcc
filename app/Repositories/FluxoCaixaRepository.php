<?php

namespace App\Repositories;

use App\Models\FluxoCaixa;
use Exception;

class FluxoCaixaRepository
{
    public function store($request) {
        try {
            return FluxoCaixa::create($request->all());
        } catch(Exception $e) {
            dd($e->getMessage());
            return false;
        }
    }

    public function getFluxoCaixa($id)
    {
        $fluxoCaixa = FluxoCaixa::with(['tipoFluxo'])->find($id);

        return $fluxoCaixa;
    }

    public function update($id, $request) {
        try {
            return FluxoCaixa::find($id)->update($request->all());
        } catch(Exception $e) {
            return false;
        }
    }

    public function all() {
        return FluxoCaixa::with(['tipoFluxo'])->orderBy('data')->get();
    }

    public function delete($id) {
        return FluxoCaixa::find($id)->delete();
    }

    public function getFluxoCaixaPorRangeData($dateI, $dateF) {
        return FluxoCaixa::with(['tipoFluxo'])->where('data', '>=', $dateI)->where('data', '<=', $dateF)->get();
    }
}
