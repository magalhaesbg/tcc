<?php

namespace App\Repositories;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ClienteRepository
{
    public function store($request) {
        try {
            $user = User::create($request->all());

            $user->assignRole('Cliente');

            return $user;
        } catch(Exception $e) {
            return false;
        }
    }

    public function getCliente($id)
    {
        $cliente = User::with(['roles'])->find($id);

        return $cliente;
    }

    public function update($id, $request) {
        try {
            User::find($id)->update($request->all());
            $user = User::with(['roles'])->where('id', $id)->get()->first();

            return $user;
        } catch(Exception $e) {
            return false;
        }
    }

    public function all() {
        return User::role(['Cliente'])->with(['roles'])->orderBy('name')->get();
    }

    public function disable($id) {
        try {
            return User::find($id)->update(['ativo' => 0]);
        } catch(Exception $e) {
            return false;
        }
    }

    public function enable($id) {
        try {
            return User::find($id)->update(['ativo' => 1]);
        } catch(Exception $e) {
            return false;
        }
    }

    public function updateSenha($request) {
        try {
            $senhaHash = Hash::make($request->password);

            return User::find( Auth::user()->id )->update(['password' => $senhaHash, 'changed_password' => date('Y-m-d H:i:s')]);
        } catch(Exception $e) {
            return false;
        }
    }

    public function getClienteInformacoes($id) {
        return User::with(['agendamento'])->find($id);
    }

    public function fidelidade() {
        return User::with(['agendamento' => function($q) {
            $q->with('fidelidade');
        }])->whereHas('agendamento.fidelidade', function($q) {
            $q->whereNull('data_resgate');
        })->get();
    }
}
