<?php

namespace App\Repositories;

use App\Models\Fidelidade;
use Exception;

class FidelidadeRepository
{
    public function store($request) {
        try {
            return Fidelidade::create($request->all());
        } catch(Exception $e) {
            return false;
        }
    }
}
