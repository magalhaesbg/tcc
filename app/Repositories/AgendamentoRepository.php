<?php

namespace App\Repositories;

use App\Models\Agendamento;
use App\Models\AgendamentoServico;
use App\Models\Ausente;
use App\Models\Feriado;
use App\Models\Horario;
use App\Models\Servico;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class AgendamentoRepository
{
    public function store($request) {
        try {
            $dados = [
                'dia_hora_inicio' => $request->dia.' '.$request->hora,
                'dia_hora_fim'    => Carbon::make($request->dia.' '.$request->hora)->addMinutes($request->tempo_estimado)->format('Y-m-d H:i'),
                'user_id'         => $request->user_id ?? Auth::user()->id,
                'barbeiro_id'     => $request->barbeiro
            ];

            if (isset($request->status)) {
                $dados = Arr::add($dados, 'status', $request->status);
            }

            $agendamento = Agendamento::create($dados);

            foreach($request->servicos AS $servico_id => $servico) {
                $servico = Servico::find($servico_id);

                AgendamentoServico::create([
                    'valor'          => $servico->valor,
                    'servico_id'     => $servico_id,
                    'agendamento_id' => $agendamento->id
                ]);
            }

            return $agendamento;
        } catch(Exception $e) {
            return false;
        }
    }

    public function getAgendamento($id)
    {
        $agendamento = Agendamento::with(['user', 'barbeiro', 'agendamentoServico'])->find($id);

        return $agendamento;
    }

    public function update($id, $request) {
        try {
            $agendamento = Agendamento::find($id)->update(array_merge(
                $request->all(),
                [
                    'dia_hora_inicio' => $request->dia.' '.$request->hora,
                    'dia_hora_fim'    => Carbon::make($request->dia.' '.$request->hora)->addMinutes($request->tempo_estimado)->format('Y-m-d H:i'),
                ]
            ));

            AgendamentoServico::where('agendamento_id', $id)->delete();

            foreach ($request->servicos AS $serviceId => $servico) {
                $servico = Servico::find($serviceId);
                AgendamentoServico::create(['agendamento_id' => $id, 'servico_id' => $serviceId, 'valor' => $servico->valor]);
            }

            return $agendamento;
        } catch(Exception $e) {
            return false;
        }
    }

    public function all() {
        if (Auth::user()->hasRole('Cliente')) {
            return Agendamento::with(['user', 'agendamentoServico', 'barbeiro'])->where('user_id', Auth::user()->id)->orderBy('dia_hora_inicio')->get();
        } else {
            return Agendamento::with(['user', 'agendamentoServico', 'barbeiro'])->orderBy('dia_hora_inicio')->get();
        }
    }

    public function getStatusAgendado() {
        if (Auth::user()->hasRole('Cliente')) {
            return Agendamento::with(['user', 'agendamentoServico', 'barbeiro'])->where('user_id', Auth::user()->id)->where('status', 'Agendado')->orderBy('dia_hora_inicio')->get();
        } else {
            return Agendamento::with(['user', 'agendamentoServico', 'barbeiro'])->orderBy('dia_hora_inicio')->get();
        }
    }

    public function getAllAgendados() {
        if (Auth::user()->hasRole('Cliente')) {
            return Agendamento::with(['user', 'agendamentoServico', 'barbeiro'])->where('user_id', Auth::user()->id)->orderBy('dia_hora_inicio')->get();
        } else {
            return Agendamento::with(['user', 'agendamentoServico', 'barbeiro'])->orderBy('dia_hora_inicio')->where('status', 'Agendado')->get();
        }
    }

    public function delete($id) {
        return Agendamento::find($id)->delete();
    }

    public function horario($request) {
        // VALIDA FERIADOS
        $feriado = Feriado::where('inicio', 'LIKE', $request->dia.'%')->get();

        if (count($feriado) > 0) {
            if ($feriado->first()->tipo_feriado_id == 1) {
                return ['status' => 'erro', 'message' => 'O dia selecionado é um feriado.'];
            }
            return ['status' => 'erro', 'message' => 'O dia selecionado está indisponível para agendamentos.'];
        }

        // VALIDA DIAS CONFIGURADOS DE TRABALHO
        $dataAgendamento = Carbon::make($request->dia);
        $diaSemanaAgendamento = $dataAgendamento->dayOfWeek;

        $diasConfigurados = Horario::with(['diaSemana'])->whereHas('diaSemana', function($q) use($diaSemanaAgendamento) {
            $q->where('ordem_semana', $diaSemanaAgendamento);
        })->get();

        if (count($diasConfigurados) <= 0) {
            return ['status' => 'erro', 'message' => 'Não abrimos no dia selecionado.'];
        }

        // VALIDA AUSENCIA DO BARBEIRO
        $ausente = Ausente::where('user_id', $request->barbeiro)->where('dia', $request->dia)->get();

        if (count($ausente) > 0) {
            return ['status' => 'erro', 'message' => 'O barbeiro selecionado está indisponível para a data selecionada.'];
        }

        // VALIDA HORARIO DOS AGENDAMENTOS
        $agendamento = Agendamento::where('barbeiro_id', $request->barbeiro)->where('dia_hora_inicio', 'LIKE', $request->dia.'%')->get();

        $tempoEstimado = $request->tempo;

        $arTMP = [];
        foreach($diasConfigurados AS $dia) {

            if (!is_null($dia->entrada_manha)) {
                $entradaManha = Carbon::make($dia->entrada_manha);
                $fimServico = Carbon::make($dia->entrada_manha);
                $saidaManha = Carbon::make($dia->saida_manha);

                while($saidaManha->gte($entradaManha)) {
                    $fimServico->addMinutes($tempoEstimado);
                    if ($saidaManha->gte($fimServico)) {
                        $arTMP[] = $entradaManha->format('H:i');
                    }
                    $entradaManha->addMinutes($tempoEstimado);
                }
            }

            if (!is_null($dia->entrada_tarde)) {
                $entradaTarde = Carbon::make($dia->entrada_tarde);
                $fimServico = Carbon::make($dia->entrada_tarde);
                $saidaTarde = Carbon::make($dia->saida_tarde);

                while($saidaTarde->gte($entradaTarde)) {
                    $fimServico->addMinutes($tempoEstimado);
                    if ($saidaTarde->gte($fimServico)) {
                        $arTMP[] = $entradaTarde->format('H:i');
                    }
                    $entradaTarde->addMinutes($tempoEstimado);
                }
            }
        }

        foreach($arTMP AS $key => $horario) {
            $hora = Carbon::make($horario);

            foreach($agendamento AS $agendado) {
                $ini = Carbon::make(substr($agendado->dia_hora_inicio, 11, 16));
                $fim = Carbon::make(substr($agendado->dia_hora_fim, 11, 16));

                if ($hora->gte($ini) && $hora->lt($fim)) {
                    unset($arTMP[$key]);
                }
            }

            if ($request->dia == date('Y-m-d')) {
                if ($hora->lt(Carbon::make(date('H:i')))) {
                    unset($arTMP[$key]);
                }
            }
        }


        return array_values(array_filter($arTMP));
    }

    public function concluido($id) {
        try {
            return Agendamento::find($id)->update(['status' => 'Concluído']);
        } catch(Exception $e) {
            return false;
        }
    }

    public function cancelado($id) {
        try {
            return Agendamento::find($id)->update(['status' => 'Cancelado']);
        } catch(Exception $e) {
            return false;
        }
    }
}
